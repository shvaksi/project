package ua.kharkov.khpi.nonka.finalTask.db;

import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public enum Role {
		CLIENT,ADMIN;
	
		public static Role getRole(User user) {
			int roleId = user.getRoleId()-1;
			return Role.values()[roleId];
		}
		public String getName() {
			return name().toLowerCase();
		}
}
