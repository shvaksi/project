package ua.kharkov.khpi.nonka.finalTask.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * @author D. Nonka
 * Apache connection pool
 */
public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	public Connection getConnection() throws SQLException {
		Connection con = null;
		try {
			Context initContext = new InitialContext();
			Context envContext = (Context) initContext.lookup("java:/comp/env");
			DataSource ds = (DataSource) envContext.lookup("jdbc/internetshopdb");
			con = ds.getConnection();
		} catch (NamingException ex) {
			System.out.println("Cannot obtain a connection from the pool" + ex);
		}
		return con;
	}

	public void commitAndClose(Connection con) {
		
			try {
				con.commit();
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		
	}

	public void rollbackAndClose(Connection con) {
		
			try {
				con.rollback();
				con.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		
	}

}
