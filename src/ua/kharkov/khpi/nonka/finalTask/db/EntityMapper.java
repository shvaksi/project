package ua.kharkov.khpi.nonka.finalTask.db;

import java.sql.ResultSet;

/**
 * @author D. Nonka
 *
 * @param <T> entity type
 */
public interface EntityMapper<T> {
	T mapRow(ResultSet rs);
}
