package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class UserDao {

	private static final String SQL__FIND_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?";
	private static final String SQL__FIND_USER_BY_ID = "SELECT * FROM users WHERE id = ?";
	private static final String SQL__ALL_USERS = "SELECT * FROM users";
	private static final String SQL__ADD_NEW_USER = "INSERT INTO users VALUES(DEFAULT,?,?,?,?,?,?,?,?,DEFAULT,?,?,?)";
	private static final String SQL__UPDATE_INFO_USER = "UPDATE users SET " + Fields.USER__FNAME + " = ? ,"
			+ Fields.USER__LNAME + " = ? ," + Fields.USER__GENDER + " = ? ," + Fields.USER__BIRTH + " = ? ,"
			+ Fields.USER__TEL + " = ? WHERE id = ?";
	private static final String SQL__UPDATE_PASSWORD_USER = "UPDATE users SET " + Fields.USER__PASSWORD
			+ " = ? WHERE id = ?";
	private static final String SQL__UPDATE_LOCALE_USER = "UPDATE users SET " + Fields.USER__LOCALE
			+ " = ? WHERE id = ?";
	private static final String SQL__UPDATE_AVATAR_USER = "UPDATE users SET " + Fields.USER__AVATAR
			+ "= ? WHERE id =? ";
	private static final String SQL__UPDATE_IS_BANNED_USER = "UPDATE users SET " + Fields.USER__ISBANNED
			+ "= ? WHERE id = ?";
	private static final String SQL__UPDATE_ROLE_USER = "UPDATE users SET " + Fields.USER__ROLEID
			+ "= ? WHERE id = ?";
	public void updateUserAvatar(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_AVATAR_USER);
			int k = 1;
			pstmt.setString(k++, user.getAvatar());
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void updateUserLocale(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_LOCALE_USER);
			int k = 1;
			pstmt.setString(k++, user.getUserLocale());
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void updateUserPassword(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_PASSWORD_USER);
			int k = 1;
			pstmt.setString(k++, user.getPassword());
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
	public void userToAdmin(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_ROLE_USER);
			int k = 1;
			pstmt.setInt(k++, 2);
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
	public void updateUserBan(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_IS_BANNED_USER);
			int k = 1;
			pstmt.setBoolean(k++, user.getIsBanned());
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}

	}

	public void updateUserInfo(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_INFO_USER);
			int k = 1;
			pstmt.setString(k++, user.getfName());
			pstmt.setString(k++, user.getlName());
			pstmt.setString(k++, user.getGender());
			pstmt.setDate(k++, new java.sql.Date(user.getBirthDate().getTime()));
			pstmt.setString(k++, user.getTelNumber());
			pstmt.setLong(k++, user.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<User> getAllUsers() {
		List<User> users = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			users = new ArrayList<User>();
			con = DBManager.getInstance().getConnection();
			UserMapper mapper = new UserMapper();
			pstmt = con.prepareStatement(SQL__ALL_USERS);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				users.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return users;
	}

	public User findUser(Long id) {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			UserMapper mapper = new UserMapper();
			pstmt = con.prepareStatement(SQL__FIND_USER_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return user;
	}

	public void insertNewUser(User user) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__ADD_NEW_USER);
			int k = 1;
			pstmt.setString(k++, user.getfName());
			pstmt.setString(k++, user.getlName());
			pstmt.setString(k++, user.getGender());
			pstmt.setDate(k++, new java.sql.Date(user.getBirthDate().getTime()));
			pstmt.setString(k++, user.getEmail());
			pstmt.setString(k++, user.getTelNumber());
			pstmt.setString(k++, user.getPassword());
			pstmt.setDate(k++, new java.sql.Date(user.getJoinDate().getTime()));
			pstmt.setBoolean(k++, user.getIsBanned());
			pstmt.setInt(k++, user.getRoleId());
			pstmt.setString(k++, user.getUserLocale());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public User findUserByEmail(String email) {
		User user = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			UserMapper mapper = new UserMapper();
			pstmt = con.prepareStatement(SQL__FIND_USER_BY_EMAIL);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				user = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return user;
	}

	private static class UserMapper implements EntityMapper<User> {

		@Override
		public User mapRow(ResultSet rs) {
			try {
				User user = new User();
				user.setId(rs.getLong(Fields.ENTITY__ID));
				user.setfName(rs.getString(Fields.USER__FNAME));
				user.setlName(rs.getString(Fields.USER__LNAME));
				user.setGender(rs.getString(Fields.USER__GENDER));
				user.setBirthDate(rs.getDate(Fields.USER__BIRTH));
				user.setEmail(rs.getString(Fields.USER__EMAIL));
				user.setTelNumber(rs.getString(Fields.USER__TEL));
				user.setPassword(rs.getString(Fields.USER__PASSWORD));
				user.setJoinDate(rs.getDate(Fields.USER__JOINDAY));
				user.setAvatar(rs.getString(Fields.USER__AVATAR));
				user.setBanned(rs.getBoolean(Fields.USER__ISBANNED));
				user.setRoleId(rs.getInt(Fields.USER__ROLEID));
				user.setUserLocale(rs.getString(Fields.USER__LOCALE));
				user.setTotalPrice(new OrderDao().getOrderSumByUser(user));
				return user;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}

}
