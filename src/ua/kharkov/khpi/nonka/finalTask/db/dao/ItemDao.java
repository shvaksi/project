package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class ItemDao {
	private static final String SQL__FIND_ITEM_BY_ID = "SELECT * FROM items WHERE id = ?";
	private static final String SQL__FIND_ITEM_BY_NAME = "SELECT * FROM items WHERE name = ?";
	private static final String SQL__ALL_ITEMS = "SELECT * FROM items";
	private static final String SQL__ADD_NEW_ITEM = "INSERT INTO items VALUES (DEFAULT,?,?,?,?,?,?,?) ";
	private static final String SQL__ADD_NEW_ITEM_WITHOUT_AVATAR = "INSERT INTO items VALUES (DEFAULT,?,DEFAULT,?,?,?,?,?) ";
	private static final String SQL__UPDATE_INFO_ITEM = "UPDATE items SET " + Fields.ITEM__NAME + " = ? ,"
			+ Fields.ITEM__PRICE + " = ? ," + Fields.ITEM__CATEGORYID + " = ? ," + Fields.ITEM__COUNTRYID + " = ? ,"
			+ Fields.ITEM__DESCRIPTION + " = ? WHERE id = ?";
	private static final String SQL__UPDATE_AVATAR_ITEM = "UPDATE items SET " + Fields.ITEM__AVATAR
			+ " = ? WHERE id = ?";
	private static final String SQL__DELETE_ITEM = "DELETE FROM items WHERE id = ?";
	private static final String SQL__NEWEST_ITEM = "SELECT * FROM items ORDER BY " + Fields.ITEM__ADD_DATE
			+ " DESC LIMIT 12";
	
	public List<Item> newestItems() {
		List<Item> items = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			items = new ArrayList<Item>();
			con = DBManager.getInstance().getConnection();
			ItemMapper mapper = new ItemMapper();
			pstmt = con.prepareStatement(SQL__NEWEST_ITEM);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}
	public List<Item> searchWithCustomQuerry(String querry) {
		List<Item> items = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			items = new ArrayList<Item>();
			con = DBManager.getInstance().getConnection();
			ItemMapper mapper = new ItemMapper();
			pstmt = con.prepareStatement(querry);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}

	public void deleteItem(Item it) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__DELETE_ITEM);
			int k = 1;
			pstmt.setLong(k++, it.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void updateItemImage(Item it) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_AVATAR_ITEM);
			int k = 1;
			pstmt.setString(k++, it.getAvatar());
			pstmt.setLong(k++, it.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void updateItemInfo(Item it) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__UPDATE_INFO_ITEM);
			int k = 1;
			pstmt.setString(k++, it.getName());
			pstmt.setInt(k++, it.getPrice());
			pstmt.setInt(k++, it.getCategory_id());
			pstmt.setInt(k++, it.getCountry_id());
			pstmt.setString(k++, it.getDescription());
			pstmt.setLong(k++, it.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public Item findById(Long id) {
		Item item = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			ItemMapper mapper = new ItemMapper();
			pstmt = con.prepareStatement(SQL__FIND_ITEM_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				item = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			item = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return item;
	}

	public Item findByName(String name) {
		Item item = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			ItemMapper mapper = new ItemMapper();
			pstmt = con.prepareStatement(SQL__FIND_ITEM_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				item = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return item;
	}

	public List<Item> getAllItems() {
		List<Item> items = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			items = new ArrayList<Item>();
			con = DBManager.getInstance().getConnection();
			ItemMapper mapper = new ItemMapper();
			pstmt = con.prepareStatement(SQL__ALL_ITEMS);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}

	private void insertItemWithoutAvatar(Item item) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__ADD_NEW_ITEM_WITHOUT_AVATAR);
			int k = 1;
			pstmt.setString(k++, item.getName());
			pstmt.setInt(k++, item.getPrice());
			pstmt.setInt(k++, item.getCategory_id());
			pstmt.setInt(k++, item.getCountry_id());
			pstmt.setString(k++, item.getDescription());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = formatter.format(item.getAddDate());
			pstmt.setString(k++, currentTime);
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void insertItem(Item item) {
		PreparedStatement pstmt = null;
		Connection con = null;
		if (item.getAvatar() != null) {
			try {
				con = DBManager.getInstance().getConnection();
				pstmt = con.prepareStatement(SQL__ADD_NEW_ITEM);
				int k = 1;
				pstmt.setString(k++, item.getName());
				pstmt.setString(k++, item.getAvatar());
				pstmt.setInt(k++, item.getPrice());
				pstmt.setInt(k++, item.getCategory_id());
				pstmt.setInt(k++, item.getCountry_id());
				pstmt.setString(k++, item.getDescription());
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String currentTime = formatter.format(item.getAddDate());
				pstmt.setString(k++, currentTime);
				pstmt.execute();
				pstmt.close();
			} catch (SQLException ex) {
				DBManager.getInstance().rollbackAndClose(con);
				ex.printStackTrace();
			} finally {
				DBManager.getInstance().commitAndClose(con);
			}
		} else {
			insertItemWithoutAvatar(item);
		}
	}

	private static class ItemMapper implements EntityMapper<Item> {

		@Override
		public Item mapRow(ResultSet rs) {
			try {
				Item item = new Item();
				item.setId(rs.getLong(Fields.ENTITY__ID));
				item.setName(rs.getString(Fields.ITEM__NAME));
				item.setAvatar(rs.getString(Fields.ITEM__AVATAR));
				item.setPrice(rs.getInt(Fields.ITEM__PRICE));
				item.setCategory_id(rs.getInt(Fields.ITEM__CATEGORYID));
				item.setCountry_id(rs.getInt(Fields.ITEM__COUNTRYID));
				item.setDescription(rs.getString(Fields.ITEM__DESCRIPTION));
				item.setAddDate(rs.getTimestamp(Fields.ITEM__ADD_DATE));
				return item;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}
}
