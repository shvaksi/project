package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Comment;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class CommentDao {
	private static final String SQL__FIND_BY_ITEM = "SELECT * FROM comments WHERE " + Fields.COMMENT__ITEM_ID + " = ?";
	private static final String SQL__DELETE_BY_ID = "DELETE FROM comments WHERE " + Fields.ENTITY__ID + " = ?";
	private static final String SQL__INSERT_COMMENT = "INSERT INTO comments VALUES(DEFAULT,?,?,?,?)";
	private static final String SQL__FIND_BY_ID = "SELECT * FROM comments WHERE id = ?";
	public Comment findById(Long id) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		Comment com = null;
		try {
			con = DBManager.getInstance().getConnection();
			CommentMapper mapper = new CommentMapper();
			pstmt = con.prepareStatement(SQL__FIND_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				com = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return com;
	}
	public void insertComment(Comment com) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__INSERT_COMMENT);
			int k = 1;
			pstmt.setInt(k++, com.getItem_id());
			pstmt.setLong(k++, com.getOwner().getId());
			pstmt.setString(k++, com.getComment());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = formatter.format(com.getAddDate());
			pstmt.setString(k++, currentTime);
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void deleteComment(Comment com) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__DELETE_BY_ID);
			int k = 1;
			pstmt.setLong(k++, com.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<Comment> findByItem(Item it) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		List<Comment> result = null;
		try {
			result = new ArrayList<Comment>();
			con = DBManager.getInstance().getConnection();
			CommentMapper mapper = new CommentMapper();
			pstmt = con.prepareStatement(SQL__FIND_BY_ITEM);
			pstmt.setLong(1,it.getId());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				result.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return result;
	}

	private static class CommentMapper implements EntityMapper<Comment> {
		@Override
		public Comment mapRow(ResultSet rs) {
			try {
				Comment comment = new Comment();
				comment.setId(rs.getLong(Fields.ENTITY__ID));
				comment.setItem_id(rs.getInt(Fields.COMMENT__ITEM_ID));
				UserDao ud = new UserDao();
				comment.setOwner(ud.findUser(rs.getLong(Fields.COMMENT__SENDER_ID)));
				comment.setComment(rs.getString(Fields.COMMENT__MESSAGE));
				comment.setAddDate(rs.getTimestamp(Fields.COMMENT__DATE));
				return comment;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}

}
