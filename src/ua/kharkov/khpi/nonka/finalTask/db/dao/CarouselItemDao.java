package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.CarouselItem;

/**
 * @author D. Nonka
 *
 */
public class CarouselItemDao {
	private static final String SQL__GET_ALL_CAROUSEL_ITEMS = "SELECT * FROM carousel_items";
	private static final String SQL__GET_MAX_CAROUSEL_ID = "SELECT * from carousel_items ORDER BY id DESC LIMIT 1";
	private static final String SQL__REMOVE_CAROUSEL_ITEM = "DELETE FROM carousel_items WHERE id = ?";
	private static final String SQL__ADD_CAROUSEL_ITEM = "INSERT INTO carousel_items VALUES(DEFAULT,?,?,?)";
	private static final String SQL__FIND_CAROUSEL_ITEM_BY_ID = "SELECT * FROM carousel_items WHERE id = ?";

	public CarouselItem findById(Long id) {
		CarouselItem item = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			CarouselMapper mapper = new CarouselMapper();
			pstmt = con.prepareStatement(SQL__FIND_CAROUSEL_ITEM_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				item = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			item = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return item;
	}

	public void addCarouselItem(CarouselItem item) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__ADD_CAROUSEL_ITEM);
			int k = 1;
			pstmt.setString(k++, item.getImage());
			pstmt.setString(k++, item.getHeader());
			pstmt.setString(k++, item.getName());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void removeCarouselItem(CarouselItem item) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__REMOVE_CAROUSEL_ITEM);
			int k = 1;
			pstmt.setLong(k++, item.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<CarouselItem> getAllCarouselItems() {
		List<CarouselItem> items = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			items = new ArrayList<CarouselItem>();
			con = DBManager.getInstance().getConnection();
			CarouselMapper mapper = new CarouselMapper();
			pstmt = con.prepareStatement(SQL__GET_ALL_CAROUSEL_ITEMS);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}

	public Long maxCarouselId() {
		CarouselItem item = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			CarouselMapper mapper = new CarouselMapper();
			pstmt = con.prepareStatement(SQL__GET_MAX_CAROUSEL_ID);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				item = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (Exception ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		if (item == null) {
			return null;
		} else {
			return item.getId();
		}

	}

	private static class CarouselMapper implements EntityMapper<CarouselItem> {

		@Override
		public CarouselItem mapRow(ResultSet rs) {
			try {
				CarouselItem cItem = new CarouselItem();
				cItem.setId(rs.getLong(Fields.ENTITY__ID));
				cItem.setImage(rs.getString(Fields.CAROUSEL__IMAGE));
				cItem.setHeader(rs.getString(Fields.CAROUSEL__HEADER));
				cItem.setName(rs.getString(Fields.CAROUSEL__NAME));
				return cItem;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}
}
