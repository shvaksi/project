package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.ItemProperty;

/**
 * @author D. Nonka
 *
 */
public class ItemPropertyDao {
	private static final String SQL__ALL_COUNTRIES = "SELECT * FROM countries";
	private static final String SQL__ALL_CATEGORIES = "SELECT * FROM categories";
	private static final String SQL__FIND_COUNTRY_BY_ID = "SELECT * FROM countries WHERE id = ?";
	private static final String SQL__FIND_CATEGORY_BY_ID = "SELECT * FROM categories WHERE id = ?";
	
	public ItemProperty findCategory(Long id) {
		ItemProperty itemp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			ItemPropertiesMapper mapper = new ItemPropertiesMapper();
			pstmt = con.prepareStatement(SQL__FIND_CATEGORY_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				itemp = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException ex){
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		}
		finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return itemp;
	}
	public ItemProperty findCountry(Long id) {
		ItemProperty itemp = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			ItemPropertiesMapper mapper = new ItemPropertiesMapper();
			pstmt = con.prepareStatement(SQL__FIND_COUNTRY_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()) {
				itemp = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException ex){
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		}
		finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return itemp;
	}
	public List<ItemProperty> getAllCategories(){
		List<ItemProperty> items = new ArrayList<ItemProperty>();;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			items = new ArrayList<ItemProperty>();
			con = DBManager.getInstance().getConnection();
			ItemPropertiesMapper mapper = new ItemPropertiesMapper();
			pstmt = con.prepareStatement(SQL__ALL_CATEGORIES);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException ex){
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		}
		finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}
	public List<ItemProperty> getAllCountries(){
		List<ItemProperty> items = new ArrayList<ItemProperty>();;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			ItemPropertiesMapper mapper = new ItemPropertiesMapper();
			pstmt = con.prepareStatement(SQL__ALL_COUNTRIES);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				items.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		}
		catch (SQLException ex){
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		}
		finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return items;
	}
	
	public static class ItemPropertiesMapper implements EntityMapper<ItemProperty>{
		
		
		@Override
		public ItemProperty mapRow(ResultSet rs) {
			try {
				ItemProperty ip = new ItemProperty();
				ip.setId(rs.getLong(Fields.ENTITY__ID));
				ip.setName(rs.getString(Fields.ITEM_PROPERTY__NAME));				
				return ip;
			}catch(SQLException e) {
				throw new IllegalStateException(e);
			}
		}
		
	}
}
