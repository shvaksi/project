package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.BasketPart;

/**
 * @author D. Nonka
 *
 */
public class BasketPartDao {
	private static final String SQL__UPDATE_BASKET_PART_COUNT_TOTAL_PRICE = "UPDATE basket_parts SET "
			+ Fields.BASKET_PART__COUNT + " = ? , " + Fields.BASKET_PART__TOTAL_PRICE + " = ? WHERE id = ?";
	private static final String SQL__DELETE_BASKET_PART = "DELETE FROM basket_parts WHERE id = ?";
	private static final String SQL__INSERT_BASKET_PART = "INSERT INTO basket_parts VALUES(DEFAULT,?,?,?,?) ";
	private static final String SQL__BASKET_PART_BY_BASKET_ID = "SELECT * FROM basket_parts WHERE "
			+ Fields.BASKET_PART__BASKET_ID + " = ?";

	public void updateBasketCount(BasketPart basketPart) {
		if (basketPart.getId() == null) {
			insertBasketPart(basketPart);
		} else {
			PreparedStatement pstmt = null;
			Connection con = null;
			try {
				con = DBManager.getInstance().getConnection();
				pstmt = con.prepareStatement(SQL__UPDATE_BASKET_PART_COUNT_TOTAL_PRICE);
				int k = 1;
				pstmt.setInt(k++, basketPart.getCount());
				pstmt.setInt(k++, basketPart.getTotalPrice());
				pstmt.setLong(k++, basketPart.getId());
				pstmt.execute();
				pstmt.close();
			} catch (SQLException ex) {
				DBManager.getInstance().rollbackAndClose(con);
				ex.printStackTrace();
			} finally {
				DBManager.getInstance().commitAndClose(con);
			}
		}
	}

	public void deleteBasketPart(Long id) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__DELETE_BASKET_PART);
			int k = 1;
			pstmt.setLong(k++, id);
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void insertBasketPart(BasketPart basketPart) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__INSERT_BASKET_PART,Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setLong(k++, basketPart.getBasket_id());
			pstmt.setLong(k++, basketPart.getItem().getId());
			pstmt.setInt(k++, basketPart.getCount());
			pstmt.setInt(k++, basketPart.getTotalPrice());
			pstmt.execute();
			ResultSet generatedKeys = pstmt.getGeneratedKeys();
			
			if(generatedKeys.next()) {
				basketPart.setId(generatedKeys.getLong(1));
			}
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<BasketPart> findByBasketId(Long id) {
		List<BasketPart> BasketPart = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			BasketPart = new ArrayList<BasketPart>();
			con = DBManager.getInstance().getConnection();
			BasketPartMapper mapper = new BasketPartMapper();
			pstmt = con.prepareStatement(SQL__BASKET_PART_BY_BASKET_ID);
			int k = 1;
			pstmt.setLong(k++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				BasketPart.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return BasketPart;

	}

	private static class BasketPartMapper implements EntityMapper<BasketPart> {
		@Override
		public BasketPart mapRow(ResultSet rs) {
			try {
				ItemDao itemDao = new ItemDao();
				BasketPart bp = new BasketPart();
				bp.setId(rs.getLong(Fields.ENTITY__ID));
				bp.setItem(itemDao.findById(rs.getLong(Fields.BASKET_PART__ITEM_ID)));
				bp.setCount(rs.getByte(Fields.BASKET_PART__COUNT));
				bp.setTotalPrice(rs.getInt(Fields.BASKET_PART__TOTAL_PRICE));
				return bp;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}

		}

	}

}
