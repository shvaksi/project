package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.OrderPart;

/**
 * @author D. Nonka
 *
 */
public class OrderPartDao {

	private static final String SQL__FIND_BY_ORDER_ID = "SELECT * FROM order_parts WHERE " + Fields.ORDER_PART__ORDER_ID
			+ " = ?";
	private static final String SQL__INSERT_ORDER_PART = "INSERT INTO order_parts VALUES(DEFAULT,?,?,?,?)";

	public void insertOrderPart(OrderPart op) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__INSERT_ORDER_PART,Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setInt(k++, op.getOrderId());
			pstmt.setLong(k++, op.getItem().getId());
			pstmt.setByte(k++, op.getCount());
			pstmt.setInt(k++, op.getTotal_price());
			pstmt.execute();
			ResultSet generatedKeys = pstmt.getGeneratedKeys();
			
			if(generatedKeys.next()) {
				op.setId(generatedKeys.getLong(1));
			}
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<OrderPart> findByOrder(Long id) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		List<OrderPart> allOrderParts = null;
		try {
			allOrderParts = new ArrayList<OrderPart>();
			con = DBManager.getInstance().getConnection();
			OrderPartMapper mapper = new OrderPartMapper();
			pstmt = con.prepareStatement(SQL__FIND_BY_ORDER_ID);
			int k = 1;
			pstmt.setLong(k++, id);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				allOrderParts.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();

		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			allOrderParts = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return allOrderParts;
	}

	private static class OrderPartMapper implements EntityMapper<OrderPart> {

		@Override
		public OrderPart mapRow(ResultSet rs) {
			try {
				OrderPart orderPart = new OrderPart();
				ItemDao itemDao = new ItemDao();
				orderPart.setId(rs.getLong(Fields.ENTITY__ID));
				orderPart.setItem(itemDao.findById(rs.getLong(Fields.ORDER_PART__ITEM_ID)));
				orderPart.setOrderId(rs.getInt(Fields.ORDER_PART__ORDER_ID));
				orderPart.setCount(rs.getByte(Fields.ORDER_PART__COUNT));
				orderPart.setTotal_price(rs.getInt(Fields.BASKET_PART__TOTAL_PRICE));
				return orderPart;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}
}
