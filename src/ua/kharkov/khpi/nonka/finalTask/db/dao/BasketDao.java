package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.mysql.jdbc.Statement;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Basket;
import ua.kharkov.khpi.nonka.finalTask.db.entity.BasketPart;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class BasketDao {
	private final static String SQL__INSERT_NEW_BASKET = "INSERT INTO baskets VALUES(DEFAULT,?)";
	private final static String SQL__FIND_BY_OWNER_ID_BASKET = "SELECT * FROM baskets WHERE " + Fields.BASKET__USER_ID
			+ " = ?";
	private final static String SQL__DELETE_BASKET = "DELETE FROM baskets WHERE id = ?";
	
	public void deleteBasket(Basket basket) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__DELETE_BASKET);
			int k = 1;
			pstmt.setLong(k++, basket.getId());			
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}
	public Basket findBasketByOwner(User user) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		Basket basket = null;
		try {
			con = DBManager.getInstance().getConnection();
			BasketMapper mapper = new BasketMapper();
			pstmt = con.prepareStatement(SQL__FIND_BY_OWNER_ID_BASKET);
			pstmt.setLong(1, user.getId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				basket = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			basket = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return basket;
	}

	public void updateBasket(Basket basket) {
		BasketPartDao bpd = new BasketPartDao();
		List<BasketPart> allBasketPart = basket.getBasketParts();
		for (BasketPart bp : allBasketPart) {
			bpd.updateBasketCount(bp);
			System.out.println("Basket Part id = "+bp.getId());
		}
	}

	public Long insertBasket(Basket basket) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__INSERT_NEW_BASKET, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setLong(k++, basket.getOwner().getId());
			pstmt.execute();
			ResultSet generatedKeys = pstmt.getGeneratedKeys();
			
			if(generatedKeys.next()) {
				return generatedKeys.getLong(1);
			}
			pstmt.close();
			
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return null;
	}

	private static class BasketMapper implements EntityMapper<Basket> {
		@Override
		public Basket mapRow(ResultSet rs) {
			try {
				UserDao ud = new UserDao();
				BasketPartDao bpd = new BasketPartDao();
				Basket basket = new Basket();
				basket.setId(rs.getLong(Fields.ENTITY__ID));
				basket.setBasketParts(bpd.findByBasketId(rs.getLong(Fields.ENTITY__ID)));
				basket.setOwner(ud.findUser(rs.getLong(Fields.BASKET__USER_ID)));
				return basket;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}
}
