package ua.kharkov.khpi.nonka.finalTask.db.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import ua.kharkov.khpi.nonka.finalTask.db.DBManager;
import ua.kharkov.khpi.nonka.finalTask.db.EntityMapper;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Order;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class OrderDao {

	private static final String SQL__ORDER_FIND_BY_ID = "SELECT * FROM orders WHERE id = ?";
	private static final String SQL__ORDER_FIND_ALL = "SELECT * FROM orders";
	private static final String SQL__INSERT_ORDER = "INSERT INTO orders VALUES(DEFAULT,?,DEFAULT,?,?)";
	private static final String SQL__ORDER_FIND_BY_CLIENT = "SELECT * FROM orders WHERE " + Fields.ORDER__CLIENT_ID
			+ " = ?";
	private static final String SQL__ORDER_UPDATE_STATUS = "UPDATE orders SET " + Fields.ORDER__STATUS
			+ " = ? WHERE id = ?";
	private static final String SQL__TOTAL_ORDER_PRICE_BY_USER = "SELECT SUM(" + Fields.ORDER__TOTAL_PRICE
			+ ") FROM orders WHERE " + Fields.ORDER__CLIENT_ID + " = ?";

	public int getOrderSumByUser(User user) {
		int total = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__TOTAL_ORDER_PRICE_BY_USER);
			pstmt.setLong(1, user.getId());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				total = rs.getInt(1);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return total;
		
	}
	public void updateStatus(Order order) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__ORDER_UPDATE_STATUS);
			int k = 1;
			pstmt.setString(k++, order.getStatus());
			pstmt.setLong(k++, order.getId());
			pstmt.execute();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public void insertOrder(Order order) {
		PreparedStatement pstmt = null;
		Connection con = null;
		try {
			con = DBManager.getInstance().getConnection();
			pstmt = con.prepareStatement(SQL__INSERT_ORDER, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setLong(k++, order.getClient().getId());
			pstmt.setInt(k++, order.getTotalPrice());
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String currentTime = formatter.format(order.getOrderDate());
			pstmt.setString(k++, currentTime);
			pstmt.execute();
			ResultSet generatedKeys = pstmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				order.setId(generatedKeys.getLong(1));
			} else {
				throw new SQLException();
			}
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
	}

	public List<Order> findOrdersByClient(User user) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		List<Order> allOrdersByClient = null;
		try {
			allOrdersByClient = new ArrayList<Order>();
			con = DBManager.getInstance().getConnection();
			OrderMapper mapper = new OrderMapper();
			pstmt = con.prepareStatement(SQL__ORDER_FIND_BY_CLIENT);
			pstmt.setLong(1, user.getId());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				allOrdersByClient.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			allOrdersByClient = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return allOrdersByClient;
	}

	public List<Order> allOrders() {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		List<Order> allOrders = null;
		try {
			allOrders = new ArrayList<Order>();
			con = DBManager.getInstance().getConnection();
			OrderMapper mapper = new OrderMapper();
			pstmt = con.prepareStatement(SQL__ORDER_FIND_ALL);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				allOrders.add(mapper.mapRow(rs));
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			allOrders = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return allOrders;
	}

	public Order findOrderById(Long id) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		Connection con = null;
		Order order = null;
		try {
			con = DBManager.getInstance().getConnection();
			OrderMapper mapper = new OrderMapper();
			pstmt = con.prepareStatement(SQL__ORDER_FIND_BY_ID);
			pstmt.setLong(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				order = mapper.mapRow(rs);
			}
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			DBManager.getInstance().rollbackAndClose(con);
			order = null;
			ex.printStackTrace();
		} finally {
			DBManager.getInstance().commitAndClose(con);
		}
		return order;
	}

	private static class OrderMapper implements EntityMapper<Order> {

		@Override
		public Order mapRow(ResultSet rs) {
			try {
				Order order = new Order();
				UserDao userDao = new UserDao();
				OrderPartDao orderPartDao = new OrderPartDao();
				order.setId(rs.getLong(Fields.ENTITY__ID));
				order.setClient(userDao.findUser(rs.getLong(Fields.ORDER__CLIENT_ID)));
				order.setStatus(rs.getString(Fields.ORDER__STATUS));
				order.setOrderParts(orderPartDao.findByOrder(order.getId()));
				order.setTotalPrice(rs.getInt(Fields.ORDER__TOTAL_PRICE));
				order.setOrderDate(rs.getTimestamp(Fields.ORDER__DATE));
				return order;
			} catch (SQLException e) {
				throw new IllegalStateException(e);
			}
		}

	}
}
