package ua.kharkov.khpi.nonka.finalTask.db;

/**
 * @author D. Nonka
 * Fields name in database tables
 */
public final class Fields {
	public static final String ENTITY__ID = "id";

	public static final String COMMENT__ITEM_ID = "item_id";
	public static final String COMMENT__SENDER_ID = "user_id";
	public static final String COMMENT__MESSAGE = "comment";
	public static final String COMMENT__DATE = "add_date";
	
	public static final String ORDER__CLIENT_ID = "client_id";
	public static final String ORDER__STATUS = "status";
	public static final String ORDER__TOTAL_PRICE = "total_price";
	public static final String ORDER__DATE = "order_date";
	
	public static final String ORDER_PART__ORDER_ID = "order_id";
	public static final String ORDER_PART__ITEM_ID = "item_id";
	public static final String ORDER_PART__COUNT = "count";
	public static final String ORDER_PART__TOTAL_PRICE = "total_price";
	
	public static final String CAROUSEL__IMAGE = "image";
	public static final String CAROUSEL__HEADER = "header";
	public static final String CAROUSEL__NAME = "name";
	
	
	public static final String BASKET_PART__BASKET_ID = "basket_id";
	public static final String BASKET_PART__ITEM_ID = "item_id";
	public static final String BASKET_PART__COUNT = "count";
	public static final String BASKET_PART__TOTAL_PRICE = "total_price";
	
	public static final String BASKET__USER_ID = "user_id";
	
	
	public static final String USER__FNAME = "first_name";
	public static final String USER__LNAME = "last_name";
	public static final String USER__GENDER = "gender";
	public static final String USER__BIRTH = "birth_day";
	public static final String USER__EMAIL = "email";
	public static final String USER__TEL = "tel_number";
	public static final String USER__PASSWORD = "password";
	public static final String USER__JOINDAY = "join_date";
	public static final String USER__AVATAR = "avatar";
	public static final String USER__ISBANNED = "isBanned";
	public static final String USER__ROLEID = "role_id";
	public static final String USER__LOCALE = "locale_name";

	public static final String ITEM__NAME = "name";
	public static final String ITEM__AVATAR = "avatar";
	public static final String ITEM__PRICE = "price";
	public static final String ITEM__CATEGORYID = "category_id";
	public static final String ITEM__COUNTRYID = "country_id";
	public static final String ITEM__DESCRIPTION = "description";
	public static final String ITEM__ADD_DATE = "add_Date";

	public static final String ITEM_PROPERTY__NAME = "name";
}
