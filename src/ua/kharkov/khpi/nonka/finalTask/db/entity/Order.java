package ua.kharkov.khpi.nonka.finalTask.db.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author D. Nonka
 *
 */
public class Order extends Entity {
	private static final long serialVersionUID = -6635332216153957117L;

	private User client;
	private List<OrderPart> orderParts;
	private String status;
	private int totalPrice;
	private Date orderDate;

	public Order() {

	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public Order(Basket basket) {
		client = basket.getOwner();
		totalPrice = basket.getTotalPrice();
		orderParts = new ArrayList<OrderPart>();
		for (BasketPart bP : basket.getBasketParts()) {
			OrderPart oP = new OrderPart();
			oP.setCount(bP.getCount());
			oP.setItem(bP.getItem());
			oP.setTotal_price(bP.getTotalPrice());
			orderParts.add(oP);
		}
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getClient() {
		return client;
	}

	public void setClient(User client) {
		this.client = client;
	}

	public List<OrderPart> getOrderParts() {
		return orderParts;
	}

	public void setOrderParts(List<OrderPart> orderParts) {
		this.orderParts = orderParts;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
