package ua.kharkov.khpi.nonka.finalTask.db.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author D. Nonka
 *
 */
public class Basket extends Entity {
	@Override
	public String toString() {
		return "Basket [basket Id = " + this.getId() + " owner=" + owner + ", basketParts size =" + basketParts.size()
				+ ", totalPrice=" + totalPrice + "]";
	}

	private static final long serialVersionUID = 1815918368810668696L;

	private User owner;
	private List<BasketPart> basketParts;
	private int totalPrice;

	public Basket() {
		basketParts = new ArrayList<BasketPart>();
	}

	public boolean plusCount(Long index) {
		for (BasketPart part : basketParts) {
			if (part.getItem().getId().equals(index)) {
				if (part.getCount() < 255) {
					part.addOneMoreCount();
					totalPrice += part.getItem().getPrice();
					return true;
				}
			}
		}
		return false;
	}

	public boolean minusCount(Long index) {
		for (BasketPart part : basketParts) {
			if (part.getItem().getId().equals(index)) {
				if (part.getCount() > 1) {
					part.minusOneMoreCount();
					totalPrice -= part.getItem().getPrice();
				}
				return true;
			}
		}
		return false;
	}

	public boolean add(BasketPart basketPart) {
		boolean added = true;
		for (BasketPart bp : basketParts) {
			if (bp.getItem().getName().equals(basketPart.getItem().getName())) {
				bp.addOneMoreCount();
				added = false;
				break;
			}
		}
		if (added) {
			basketParts.add(basketPart);
		}
		totalPrice += basketPart.getTotalPrice();
		return added;
	}

	public Long remove(BasketPart basketPart) {
		Long result = null;
		for (BasketPart bp : basketParts) {
			if (bp.getItem().getName().equals(basketPart.getItem().getName())) {
				int itemPartPrice = bp.getTotalPrice();
				result = bp.getId();
				basketParts.remove(bp);
				totalPrice -= itemPartPrice;
			}
		}
		return result;
	}

	public Long remove(Long basket_id) {
		Long result = null;
		for (BasketPart bp : basketParts) {
			if (bp.getItem().getId().equals(basket_id)) {
				int itemPartPrice = bp.getTotalPrice();
				result = bp.getId();
				basketParts.remove(bp);
				totalPrice -= itemPartPrice;
				break;
			}
		}
		return result;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public List<BasketPart> getBasketParts() {
		return basketParts;
	}

	public void setBasketParts(List<BasketPart> basketParts) {
		this.basketParts = basketParts;
	}

}
