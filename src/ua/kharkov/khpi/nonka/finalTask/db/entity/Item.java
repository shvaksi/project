package ua.kharkov.khpi.nonka.finalTask.db.entity;

import java.util.Comparator;
import java.util.Date;

/**
 * @author D. Nonka
 *
 */
public class Item extends Entity {
	private static final long serialVersionUID = 5675788709224283293L;

	private String name;
	private String avatar;
	private int price;
	private int category_id;
	private int country_id;
	private String description;
	private Date addDate;

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getCountry_id() {
		return country_id;
	}

	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}

	public static final class Comparators{
		public static final Comparator<Item> NAME = (Item arg0, Item arg1) -> arg0.getName().compareTo(arg1.getName());
		public static final Comparator<Item> PRICE = (Item arg0, Item arg1) -> Integer.compare(arg0.getPrice(), arg1.getPrice());
		public static final Comparator<Item> ADD_DATE = (Item arg0, Item arg1) -> arg0.getAddDate().compareTo(arg1.getAddDate());
	}
}
