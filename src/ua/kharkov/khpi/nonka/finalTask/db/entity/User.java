package ua.kharkov.khpi.nonka.finalTask.db.entity;

import java.util.Date;

/**
 * @author D. Nonka
 *
 */
public class User extends Entity {
	private static final long serialVersionUID = -6889036256149495388L;

	private String fName;
	private String lName;
	private String gender;
	private Date birthDate;
	private String email;
	private String telNumber;
	private String password;
	private Date joinDate;
	private String avatar;
	private boolean isBanned;
	private int roleId;
	private String userLocale;
	private int totalPrice;
	
	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getUserLocale() {
		return userLocale;
	}

	public void setUserLocale(String userLocale) {
		this.userLocale = userLocale;
	}

	public String getfName() {
		return fName;
	}

	public void setfName(String fName) {
		this.fName = fName;
	}

	public String getlName() {
		return lName;
	}

	public void setlName(String lName) {
		this.lName = lName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public boolean getIsBanned() {
		return isBanned;
	}

	public void setBanned(boolean isBanned) {
		this.isBanned = isBanned;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	@Override
	public String toString() {
		return "User [lName=" + lName + ", fName=" + fName + ", gender=" + gender + ", birthDate=" + birthDate
				+ ", email=" + email + ", telNumber=" + telNumber + ", password=" + password + ", joinDate=" + joinDate
				+ ", avatar=" + avatar + ", isBanned=" + isBanned + ", roleId" + roleId + ", userLocale=" + userLocale
				+ "]";
	}

}
