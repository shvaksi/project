package ua.kharkov.khpi.nonka.finalTask.db.entity;

/**
 * @author D. Nonka
 *
 */
public class BasketPart extends Entity {
	@Override
	public String toString() {
		return "BasketPart [basket_id=" + basket_id + ", item=" + item + ", count=" + count + ", totalPrice="
				+ totalPrice + "]";
	}

	private static final long serialVersionUID = -7686976190510666380L;
	private int basket_id;
	private Item item;
	private byte count = 1;
	private int totalPrice;
	
	public void addOneMoreCount() {
		count++;
		setTotalPrice();
	}
	public void minusOneMoreCount() {
		if(count > 1) {
			count--;
			setTotalPrice();
		}
	}
	public int getBasket_id() {
		return basket_id;
	}

	public void setBasket_id(int basket_id) {
		this.basket_id = basket_id;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public byte getCount() {
		return count;
	}

	public void setCount(byte count) {
		this.count = count;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice() {
		totalPrice = item.getPrice() * count;
	}

	public void setTotalPrice(int total_price) {
		this.totalPrice = total_price;
	}
}
