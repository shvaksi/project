package ua.kharkov.khpi.nonka.finalTask.db.entity;

/**
 * @author D. Nonka
 *
 */
public class ItemProperty extends Entity{
	private static final long serialVersionUID = -5547813725284925452L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
