package ua.kharkov.khpi.nonka.finalTask.db.entity;

import java.util.Date;

/**
 * @author D. Nonka
 *
 */
public class Comment extends Entity {
	private static final long serialVersionUID = 5709342515869884622L;

	private int item_id;
	private User owner;
	private String comment;
	private Date addDate;

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Date getAddDate() {
		return addDate;
	}

	public void setAddDate(Date addDate) {
		this.addDate = addDate;
	}

}
