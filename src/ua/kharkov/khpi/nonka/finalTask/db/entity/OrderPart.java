package ua.kharkov.khpi.nonka.finalTask.db.entity;

/**
 * @author D. Nonka
 *
 */
public class OrderPart extends Entity{
	private static final long serialVersionUID = -7368872318620090253L;
	
	private int orderId;
	private Item item;
	private byte count;
	private int total_price;
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public byte getCount() {
		return count;
	}
	public void setCount(byte count) {
		this.count = count;
	}
	public int getTotal_price() {
		return total_price;
	}
	public void setTotal_price(int total_price) {
		this.total_price = total_price;
	}
}
