package ua.kharkov.khpi.nonka.finalTask.db.entity;

/**
 * @author D. Nonka
 *
 */
public class CarouselItem extends Entity{
	private static final long serialVersionUID = 2859044197869863907L;
	
	private String image;
	private String header;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
}
