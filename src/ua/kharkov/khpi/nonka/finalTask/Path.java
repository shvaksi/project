package ua.kharkov.khpi.nonka.finalTask;

public final class Path {
	
	public static final String PAGE__INDEX = "index.jsp";
	public static final String PAGE__USER_PROFILE = "/WEB-INF/jsp/userProfile.jsp";
	public static final String PAGE__LOGIN = "login.jsp";
	public static final String PAGE__REGISTRY = "registry.jsp";
	public static final String PAGE__ITEM_CREATE = "itemCreate.jsp";
	public static final String PAGE__ERROR_PAGE = "/WEB-INF/jsp/error_page.jsp";
	public static final String PAGE__UPDATE_ITEM_PAGE = "/WEB-INF/jsp/itemUpdate.jsp";
	public static final String PAGE__ITEM_PAGE = "/WEB-INF/jsp/itemPage.jsp";
	public static final String PAGE__CATALOG = "/WEB-INF/jsp/catalog.jsp";
			
	// commands
	public static final String COMMAND__CATALOG	=	"controller?command=catalog";
	public static final String COMMAND__ITEM_PAGE = "controller?command=itemPage";
	public static final String COMMAND__LOGIN = "/controller?command=loginUser";
	public static final String COMMAND__REGISTRY = "/controller?command=registerUser";
	public static final String COMMAND__USER_PROFILE = "controller?command=userProfile";
	public static final String COMMAND__UPDATE_ITEM_INFO = "controller?command=updateItemInfo&target_item=";

}
