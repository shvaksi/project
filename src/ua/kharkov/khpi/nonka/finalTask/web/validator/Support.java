package ua.kharkov.khpi.nonka.finalTask.web.validator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * @author D. Nonka
 *	Utilitarian class with static methods for validation or upload
 */
public final class Support {
	private static final int MIN_AGE = 18;
	private static final int MAX_AGE = 120;
	private static final int maxFileSize = (int) (5 * Math.pow(10, 6));
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	private static List<String> errors = new ArrayList<String>();
	

	static {
		errors.add("loginErrorMessage");
		errors.add("error_map");
		errors.add("error_upadates");
		errors.add("error_upadates_password");
		errors.add("user_update");
		errors.add("addItemErrors");
	}
	
	/**
	 * @param input
	 * @return encoded input
	 */
	public static String encoding(String input) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(input.getBytes());
			byte[] hash = digest.digest();
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < hash.length; j++) {
				String s = Integer.toHexString(0xff & hash[j]);
				s = (s.length() == 1) ? "0" + s : s;
				sb.append(s);
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return input;
		}
	}

	/**
	 * @param original
	 * @param compare
	 * @return true if original text eq encoded compare
	 */
	public static boolean matching(String original, String compare) {
		  String md5 = null;
		    try{
		        MessageDigest md = MessageDigest.getInstance("MD5");
		        md.update(compare.getBytes());
		        byte[] digest = md.digest();
		        md5 = new BigInteger(1, digest).toString(16);

		        return md5.equals(original);

		    } catch (NoSuchAlgorithmException e) {
		        return false;
		    }
	}
	/**
	 * @param part
	 * @return converted part to String 
	 */
	public static String partToString(Part part) {
		Scanner s = null;
		try {
			s = new Scanner(part.getInputStream(),"UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		String answ = null;
		if (s.hasNextLine()) {
			answ = s.nextLine();
		}
		s.close();
		return answ;
	}

	/**
	 * @param part
	 * @return file name from part
	 */
	private static String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	/**
	 * @param request
	 * @param imgPart
	 * @param filePath
	 * @return upload file from imgPart in filePath and validate 
	 */
	public static String uploadFile(HttpServletRequest request, Part imgPart, String filePath) {
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		String avatarName = null;
		if (!isMultipart) {
			System.out.println("DON'T multipart content");
			return avatarName;
		}
		try {
			System.out.println("In try scope");
			File appPath = new File(request.getServletContext().getRealPath("\\"));
			String savePath = request.getSession().getServletContext().getAttribute("uploadLocation") + filePath;
			System.out.println("Save Path --> " + savePath);
			File fileSaveDir = new File(savePath);
			System.out.println("File save dir" + fileSaveDir.toString());
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdirs();
			}
			System.out.println("File name:" + extractFileName(imgPart));
			String[] array = extractFileName(imgPart).split("\\.");
			System.out.println("Array length:" + array.length);
			for (int i = 0; i < array.length; i++) {
				System.out.println("Array part " + i + ":" + array[i]);
			}
			String format = null;

			switch (array[array.length - 1]) {
			case "png":
				format = "png";
				break;
			case "jpg":
				format = "jpg";
				break;
			case "jpeg":
				format = "jpeg";
				break;
			}
			System.out.println("Format is " + format);
			if (format == null) {
				return avatarName;
			}
			String newFileName = UUID.randomUUID().toString();
			System.out.println("File is: " + fileSaveDir.toString() + File.separator + newFileName + "." + format);
			if (imgPart.getSize() > maxFileSize) {
				return null;
			}
			InputStream input = imgPart.getInputStream();

			Files.copy(input, new File(appPath.toString() + File.separator + fileSaveDir.toString() + File.separator
					+ newFileName + "." + format).toPath(), StandardCopyOption.REPLACE_EXISTING);
			avatarName = new File(fileSaveDir.toString()).toString() + File.separator + newFileName + "." + format;
			System.out.println("Upload has been done successfully!");

			return avatarName;
		} catch (Exception e) {
			System.out.println("ERROR in method");
			e.printStackTrace();
		}

		return avatarName;
	}

	/**
	 * @param request
	 * Clear errors messages
	 */
	public static void clearErrorMessages(HttpServletRequest request) {
		HttpSession session = request.getSession();
		for (String s : errors) {
			session.removeAttribute(s);
		}
	}

	/**
	 * @param str
	 * @param template
	 * @param myBundle
	 * @param incorrect
	 * @return error message if validation failed or null if validation success
	 */
	public static String validateText(String str, String template, ResourceBundle myBundle, String incorrect) {
		if (str == null || str.isEmpty()) {
			String error = myBundle.getString("Field.Required.Empty");
			return error;
		} else {
			if (str.matches(template)) {
				return null;
			} else {
				String error = myBundle.getString(incorrect) + " " + template;
				return error;
			}
		}
	}

	/**
	 * @param password
	 * @param confirmPassword
	 * @param template
	 * @param myBundle
	 * @return error message if validate faild or null if validate success
	 */
	public static String validatePasswords(String password, String confirmPassword, String template,
			ResourceBundle myBundle) {
		if ((password == null || password.isEmpty()) || (confirmPassword == null || confirmPassword.isEmpty())) {
			String error = myBundle.getString("Field.Required.Empty");
			return error;
		} else {
			if (password.matches(template) && password.equals(confirmPassword)) {
				return null;
			} else if (!password.matches(template)) {
				String error = myBundle.getString("Registry.Fields.Incorrect") + " " + template;
				return error;
			} else {
				String error = myBundle.getString("Registry.Password.DoNotMacth");
				return error;
			}
		}
	}

	/**
	 * @param date
	 * @param bundle
	 * @return error message if validate faild or null if validate success
	 */
	public static String validateDate(String date, ResourceBundle bundle) {
		Date mDate = null;
		try {
			mDate = SDF.parse(date);
		} catch (ParseException e) {
			return bundle.getString("Registry.Date.Icorrect");
		}
		// maxDate - before this date our date is valid
		Calendar lowerDate = Calendar.getInstance();
		lowerDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) - (MIN_AGE + 1));
		Date maxDate = lowerDate.getTime();
		System.out.println(SDF.format(maxDate));

		// minDate - after this date our date is valid
		Calendar upperDate = Calendar.getInstance();
		upperDate.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR) - (MAX_AGE - 1));
		Date minDate = upperDate.getTime();
		System.out.println(SDF.format(minDate));
		System.out.println(SDF.format(mDate));

		if (mDate.after(minDate) && mDate.before(maxDate)) {
			return null;
		} else if (mDate.after(minDate)) {
			return bundle.getString("Registry.Date.Higher") + " " + SDF.format(maxDate);
		} else {
			return bundle.getString("Registry.Date.Lower") + " " + SDF.format(minDate);
		}
	}
}
