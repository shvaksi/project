package ua.kharkov.khpi.nonka.finalTask.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.web.command.Command;
import ua.kharkov.khpi.nonka.finalTask.web.command.CommandContainer;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;
/**
 * @author D. Nonka
 *
 */
@MultipartConfig
public class Controller extends HttpServlet {
	private static final long serialVersionUID = -894443209033050109L;
	private static final Logger log = Logger.getLogger(Controller.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		process(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("redirect", "true");
		Support.clearErrorMessages(request);
		process(request, response);
	}

	private void process(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.trace("Controller procces...");
		String commandName = request.getParameter("command");
		Command command = CommandContainer.get(commandName);
		String forward = command.execute(request, response);
		log.trace("Controller forward: " + forward);
		if (forward != null) {
			if (request.getAttribute("redirect") != null) {
				log.trace("Redirect to " + forward);
				response.sendRedirect(forward);
			} else {
				log.trace("Forward tp " + forward);
				RequestDispatcher disp = request.getRequestDispatcher(forward);
				disp.forward(request, response);
			}
		}

	}

}
