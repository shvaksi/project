package ua.kharkov.khpi.nonka.finalTask.web.tags;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.kharkov.khpi.nonka.finalTask.db.dao.CarouselItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;


/**
 * @author Imagimaru
 * Custom tag for jsp
 */
public class MyTag extends SimpleTagSupport  {

	 public void doTag() throws JspException, IOException {
		 getJspContext().setAttribute("CarouselItemList",new CarouselItemDao().getAllCarouselItems());
		 getJspContext().setAttribute("newestItems", new ItemDao().newestItems());
	   }
}
