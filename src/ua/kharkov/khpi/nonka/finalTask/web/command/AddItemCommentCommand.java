package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.CommentDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Comment;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class AddItemCommentCommand extends Command{
	private static final long serialVersionUID = -7495859025052188035L;
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String forward = request.getHeader("Referer");
		request.setAttribute("redirect", "true");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Long targ_item = null;
		String comment = null;
		User user = (User) request.getSession().getAttribute("user");
		ItemDao itemDao = new ItemDao();
		try {
			targ_item = Long.valueOf(request.getParameter("targ-item"));
			comment = request.getParameter("comment");
			if(targ_item == null || itemDao.findById(targ_item) == null) {
				throw new Exception("Invalid target item id "+targ_item);
			}
			if(comment.isEmpty() || comment.length() > 4000) {
				throw new Exception("Invalid comment");
			}
			Comment com = new Comment();
			com.setAddDate(new Date());
			com.setComment(comment);
			com.setItem_id(targ_item.intValue());
			com.setOwner(user);			
			CommentDao comDao = new CommentDao();
			comDao.insertComment(com);
			return forward;
		}catch(Exception e) {
			request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
