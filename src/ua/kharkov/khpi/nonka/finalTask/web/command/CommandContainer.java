package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;

/**
 * @author D. Nonka
 *
 */
public class CommandContainer {
	private static final Logger log = Logger.getLogger(CommandContainer.class);

	private static Map<String, Command> commands = new TreeMap<String, Command>();
	static {
		commands.put("loginUser", new LoginCommand());
		commands.put("registerUser", new RegistryCommand());
		commands.put("logout", new LogoutCommand());
		commands.put("noCommand", new NoCommand());
		commands.put("userProfile", new UserProfileCommand());
		commands.put("userInfoUpdate", new UpdateUserInfoCommand());
		commands.put("userUpdatePassword", new UpdateUserPasswordCommand());
		commands.put("cancelUpdateUser", new CancelUpdateUserCommand());
		commands.put("changeLanguage", new ChangeUserLocaleCommand());
		commands.put("addItem", new AddItemCommand());
		commands.put("updateItemInfo", new UpdateItemInfoCommand());
		commands.put("userAvatarUpdate", new UpdateUserImageCommand());
		commands.put("updateItemImage", new UpdateItemImageCommand());
		commands.put("itemUpdate", new ItemUpdatePageCommand());
		commands.put("itemDelete", new DeleteItemCommand());
		commands.put("itemPage", new ItemPageCommand());
		commands.put("addCarouselItem", new AddCarouselItemCommand() );
		commands.put("removeCarouselItem", new RemoveCarouselItem());
		commands.put("banChange", new BanChangeCommand());
		commands.put("upgradeUser", new UpgradeToAdminCommand());
		commands.put("catalog", new CatalogPageCommand());
		commands.put("searchItem", new SearchItemCommand());
		commands.put("filterItem", new ItemFilterCommand());
		commands.put("catalogSort", new SortCatalogItemsCommand());
		commands.put("basketAddItem", new BasketAddItem());
		commands.put("changeConditionBasket", new ChangeConditionBasketPartCommand());
		commands.put("addOrder", new AddOrderCommand());
		commands.put("setOrderStatus", new OrderStatusChangeCommand());
		commands.put("addComment", new AddItemCommentCommand());
		commands.put("deleteComment", new DeleteCommentCommand());
	}

	public static Command get(String commandName) {
		log.trace("Read command --> " + commandName);
		if (commandName == null || !commands.containsKey(commandName)) {
			log.error("Command not found, name --> " + commandName);
			return commands.get("noCommand");
		}
		return commands.get(commandName);
	}

}
