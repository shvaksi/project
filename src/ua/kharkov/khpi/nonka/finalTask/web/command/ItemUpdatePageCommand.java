package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class ItemUpdatePageCommand extends Command {
	private static final long serialVersionUID = 7408586573529407331L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long id = null;
		try {
			id = Long.valueOf(request.getParameter("targ-item"));
		} catch (Exception e) {
			request.setAttribute("message", e.getMessage());
			return Path.PAGE__ERROR_PAGE;
		}
		ItemDao itemDao = new ItemDao();
		Item item = null;
		item = itemDao.findById(id);
		if (item == null) {
			request.setAttribute("errorMessage", "Invalid item id");
			return Path.PAGE__ERROR_PAGE;
		} else {
			System.out.println(item.getName());
			request.setAttribute("item", item);
			return Path.PAGE__UPDATE_ITEM_PAGE;
		}

	}

}
