package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketPartDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Basket;
import ua.kharkov.khpi.nonka.finalTask.db.entity.BasketPart;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class BasketAddItem extends Command {
	private static final long serialVersionUID = -724206637719276286L;
	private static final Logger log = Logger.getLogger(BasketAddItem.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start...");
		String forward = request.getHeader("Referer");
		request.setAttribute("redirect", "true");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Long item_id = null;
		Item item = null;
		try {
			item_id = Long.valueOf(request.getParameter("targ-item"));
			ItemDao itemDao = new ItemDao();
			item = itemDao.findById(item_id);
			if (item == null) {
				throw new Exception();
			}
			log.debug("Variable init --> success");
		} catch (Exception e) {
			request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
			return Path.PAGE__ERROR_PAGE;
		}
		User user = (User) request.getSession().getAttribute("user");
		BasketPart basketPart = new BasketPart();
		basketPart.setItem(item);
		basketPart.setTotalPrice();
		Basket basket = (Basket) request.getSession().getAttribute("userBasket");
		if (basket == null) {
			log.debug("Create new basket");
			basket = new Basket();
			if (user != null) {
				BasketDao bd = new BasketDao();
				basket.setOwner(user);
				basket.setId(bd.insertBasket(basket));
				basketPart.setBasket_id(basket.getId().intValue());
				BasketPartDao bpd = new BasketPartDao();
				bpd.insertBasketPart(basketPart);
				System.out.println("Inserting basket.... Basket id = " + basketPart.getBasket_id());
			}
			basket.add(basketPart);

		} else {

			if (user != null) {
				basketPart.setBasket_id(basket.getId().intValue());
				if (basket.getOwner() == null) {
					basket.setOwner(user);
				}
			}
			basket.add(basketPart);
			if (user != null) {
				BasketDao bd = new BasketDao();
				bd.updateBasket(basket);

			}
		}

		request.getSession().setAttribute("userBasket", basket);
		return forward;
	}

}
