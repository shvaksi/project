package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author D. Nonka
 *
 */
public abstract class Command implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public abstract String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException;
	
	@Override
	public final String toString() {
		return getClass().getSimpleName();
	}


}
