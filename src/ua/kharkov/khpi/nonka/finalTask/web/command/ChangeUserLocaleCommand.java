package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;

import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class ChangeUserLocaleCommand extends Command {

	private static final long serialVersionUID = -8750783732263776223L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String forward = request.getHeader("Referer");
		request.setAttribute("redirect", "true");
		User user = (User) request.getSession().getAttribute("user");
		String target_locale = request.getParameter("target_locale");
		String defLocale = (String) request.getSession().getAttribute("defaultLocale");
		if (target_locale == null) {			
			defLocale = (defLocale.equals("ru")) ? "en" : "ru";
			if (user != null) {
				user.setUserLocale(defLocale);
				UserDao ud = new UserDao();
				ud.updateUserLocale(user);
				request.getSession().setAttribute("user",user);
			}
			Config.set(request.getSession(), "javax.servlet.jsp.jstl.fmt.locale", defLocale);
			request.getSession().setAttribute("defaultLocale", defLocale);
		}else if(target_locale.equals(defLocale)){
			return forward;
		}else {
			defLocale = (target_locale.equals("ru")) ? "ru" : "en";
			if (user != null) {
				user.setUserLocale(defLocale);								
				request.getSession().setAttribute("user",user);
			}
			Config.set(request.getSession(), "javax.servlet.jsp.jstl.fmt.locale",defLocale);
			request.getSession().setAttribute("defaultLocale", defLocale);
		}
		return forward;		
	}

}
