package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class DeleteItemCommand extends Command{
	private static final long serialVersionUID = -1013096300331046750L;
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long id = null;
		try {
			id= Long.valueOf(request.getParameter("targ-item"));
		}
		catch(Exception e) {
			request.setAttribute("message", "Invalid parameter");
			return Path.PAGE__ERROR_PAGE;
		}
		ItemDao itemDao = new ItemDao();
		Item item = itemDao.findById(id);
		if(item != null) {
			itemDao.deleteItem(item);
			request.setAttribute("redirect", "true");
			return Path.PAGE__INDEX;
		}else {
			request.setAttribute("message", "Invalid item id");
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
