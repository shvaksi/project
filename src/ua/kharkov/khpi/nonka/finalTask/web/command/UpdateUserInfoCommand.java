package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class UpdateUserInfoCommand extends Command {
	private static final long serialVersionUID = -1786287260791927619L;
	private static final Logger log = Logger.getLogger(UpdateUserInfoCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Map<String, String> error_map = new HashMap<String, String>();
		error_map.put(String.valueOf(0),
				Support.validateText(request.getParameter("first_name"), FieldTemplate.TEMPLATE__USER_NAME, bundle,"Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(1),
				Support.validateText(request.getParameter("last_name"), FieldTemplate.TEMPLATE__USER_NAME, bundle,"Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(2),
				Support.validateText(request.getParameter("gender"), FieldTemplate.TEMPLATE__USER_GENDER, bundle,"Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(3), Support.validateDate(request.getParameter("birthday"), bundle));
		error_map.put(String.valueOf(4),
				Support.validateText(request.getParameter("phone_number"), FieldTemplate.TEMPLATE__USER_NUMBER, bundle,"Registry.Fields.Incorrect"));

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		String forward = Path.COMMAND__USER_PROFILE;
		User user = new User();
		user.setfName(request.getParameter("first_name"));
		user.setlName(request.getParameter("last_name"));
		user.setGender(request.getParameter("gender"));
		try {
			user.setBirthDate(formatter.parse(request.getParameter("birthday")));
		} catch (ParseException e) {
			user.setBirthDate(null);
			e.printStackTrace();
		}
		user.setTelNumber(request.getParameter("phone_number"));
		request.getSession().setAttribute("error_upadates", error_map);
		boolean isDone = true;
		for (int i = 0; i < error_map.size(); i++) {
			if (error_map.get(String.valueOf(i)) != null) {
				log.debug("Validation --> NO");
				isDone = false;
				break;
			}
		}
		if (isDone) {
			log.debug("Validation --> OK");			
			UserDao ud = new UserDao();
			User curUser = (User) request.getSession().getAttribute("user");
			user.setId(curUser.getId());
			ud.updateUserInfo(user);
			log.debug("User was updated -->"+user.getfName());
			curUser = ud.findUser(curUser.getId());
			request.getSession().setAttribute("user", curUser);
			request.getSession().removeAttribute("error_upadates");
			request.getSession().removeAttribute("user_update");
		}else {
			request.getSession().setAttribute("user_update",user);
		}

		return forward;
	}

}
