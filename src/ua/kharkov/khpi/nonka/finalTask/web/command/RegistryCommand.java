package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class RegistryCommand extends Command {
	private static final long serialVersionUID = 2918663095052776417L;
	private static final Logger log = Logger.getLogger(RegistryCommand.class);
	private final static int REGISTRY_FIELDS_NUMBER = 7;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");

		String def_locale = (String) request.getSession().getAttribute("defaultLocale");

		if (def_locale == null) {
			def_locale = "ru";
		}
		ResourceBundle myBundle = ResourceBundle.getBundle("resources");
		// obtain user form
		Map<String, String> error_map = new HashMap<String, String>();

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String forward = Path.PAGE__REGISTRY;
		User user = new User();
		UserDao ud = new UserDao();

		error_map.put(String.valueOf(0), Support.validateText(request.getParameter("first_name"),
				FieldTemplate.TEMPLATE__USER_NAME, myBundle, "Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(1), Support.validateText(request.getParameter("last_name"),
				FieldTemplate.TEMPLATE__USER_NAME, myBundle, "Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(2), Support.validateText(request.getParameter("gender"),
				FieldTemplate.TEMPLATE__USER_GENDER, myBundle, "Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(3), Support.validateDate(request.getParameter("birthday"), myBundle));
		if (ud.findUserByEmail(request.getParameter("email")) != null) {
			error_map.put(String.valueOf(4), myBundle.getString("Registry.Email.AlreadyExist"));
		} else {
			error_map.put(String.valueOf(4), Support.validateText(request.getParameter("email"),
					FieldTemplate.TEMPLATE__USER_EMAIL, myBundle, "Registry.Fields.Incorrect"));
		}
		error_map.put(String.valueOf(5), Support.validateText(request.getParameter("phone_number"),
				FieldTemplate.TEMPLATE__USER_NUMBER, myBundle, "Registry.Fields.Incorrect"));
		error_map.put(String.valueOf(6), Support.validatePasswords(request.getParameter("password"),
				request.getParameter("confirm_password"), FieldTemplate.TEMPLATE__USER_PASSWORD, myBundle));

		request.getSession().setAttribute("error_map", error_map);

		for (int i = 0; i < REGISTRY_FIELDS_NUMBER; i++) {
			if (error_map.get(String.valueOf(i)) == null) {
				continue;
			} else {
				return forward;
			}
		}
		user.setPassword(Support.encoding(request.getParameter("password")));
		user.setfName(request.getParameter("first_name"));
		user.setlName(request.getParameter("last_name"));
		user.setGender(request.getParameter("gender"));
		user.setEmail(request.getParameter("email"));
		user.setTelNumber(request.getParameter("phone_number"));
		SimpleDateFormat simp = new SimpleDateFormat("yyyy-MM-dd");
		Date date;
		try {
			date = simp.parse(request.getParameter("birthday"));
			System.out.println(date.getTime());
		} catch (ParseException e1) {	
			e1.printStackTrace();
			request.setAttribute("redirect", "true");
			request.setAttribute("errorMessage", e1.getMessage());
			return Path.PAGE__ERROR_PAGE;
		}
		user.setBirthDate(date);
		Date today = new Date();
		try {
			Date todayWithZeroTime = formatter.parse(formatter.format(today));
			user.setJoinDate(todayWithZeroTime);
			user.setUserLocale(def_locale);
			user.setRoleId(new Integer(1));
		} catch (ParseException e) {
			e.printStackTrace();
			return forward;
		}

		ud.insertNewUser(user);
		request.removeAttribute("redirect");
		forward = Path.COMMAND__LOGIN + "&email=" + user.getEmail() + "&password=" + request.getParameter("password");
		return forward;
	}

}
