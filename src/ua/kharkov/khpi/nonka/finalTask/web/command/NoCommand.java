package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;

/**
 * @author D. Nonka
 *
 */
public class NoCommand extends Command {
	private static final long serialVersionUID = -5900954525017337640L;

	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		String errorMessage = "No such command";
		request.setAttribute("errorMessage", errorMessage);

		return Path.PAGE__ERROR_PAGE;
	}

}
