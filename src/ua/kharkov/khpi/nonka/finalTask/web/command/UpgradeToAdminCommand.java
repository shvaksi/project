package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class UpgradeToAdminCommand extends Command{
	private static final long serialVersionUID = -7196725216453673314L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long id = null;
		try {
			id = Long.valueOf(request.getParameter("targ-user"));
		} catch (Exception e) {
			request.setAttribute("errorMessage", "Invalide parameter");
			return Path.PAGE__ERROR_PAGE;
		}
		UserDao ud = new UserDao();
		User user = ud.findUser(id);
		if (user != null) {
			if(!user.getIsBanned()) {
				ud.userToAdmin(user);
				user = ud.findUser(id);
				Map<Long, User> userChangingMap = (Map<Long, User>) request.getSession().getServletContext().getAttribute("userChangingMap");
				if(userChangingMap != null) {
					userChangingMap.put(user.getId(), user);
				}else {
					userChangingMap = new HashMap<Long,User>();
					userChangingMap.put(user.getId(), user);
				}
				request.getSession().getServletContext().setAttribute("userChangingMap", userChangingMap);
				request.setAttribute("redirect", "true");
				return Path.COMMAND__USER_PROFILE;
			}else {
				request.setAttribute("errorMessage", "User "+user.getEmail()+" is banned!\nYou should unban him to upgrade.");
				return Path.PAGE__ERROR_PAGE;
			}
		}else {
			request.setAttribute("errorMessage", "Invalid user id");
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
