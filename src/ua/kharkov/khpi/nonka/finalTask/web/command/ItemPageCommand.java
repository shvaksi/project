package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.CommentDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class ItemPageCommand extends Command{
	private static final long serialVersionUID = 3843915598998742735L;
	private static final Logger log = Logger.getLogger(ItemPageCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start");
		
		
		String forward = Path.PAGE__ERROR_PAGE;
		Long id = null;
		Item item = null;
		ItemDao itemDao = new ItemDao();
		try {
			id = Long.valueOf(request.getParameter("targ-item"));
			item = itemDao.findById(id);			
		}
		catch(NullPointerException e){
			e.printStackTrace();
		}
		if (item == null) {
			request.getSession().setAttribute("errorMessage", "Unknown item with id "+request.getParameter("targ-item"));
			return forward;
		}
		CommentDao comDao = new CommentDao();
		request.setAttribute("itemComments", comDao.findByItem(item));
		log.debug("Return item "+item.getId());
		request.setAttribute("item", item);
		return Path.PAGE__ITEM_PAGE;
	}

}
