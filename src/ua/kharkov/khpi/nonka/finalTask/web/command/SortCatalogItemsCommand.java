package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Collections;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class SortCatalogItemsCommand extends Command {
	private static final long serialVersionUID = 580469476938503807L;
	private static final Logger log = Logger.getLogger(SortCatalogItemsCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start");
		String sortBy = request.getParameter("sortBy");
		String reverse = request.getParameter("reverse");
		List<Item> itemCatalog = (List<Item>) request.getSession().getAttribute("itemCatalog");
		if (itemCatalog != null && !itemCatalog.isEmpty()) {
			ResourceBundle bundle = ResourceBundle.getBundle("resources");
			if (sortBy == null) {
				request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
				return Path.PAGE__ERROR_PAGE;
			}
			if (itemCatalog.size() > 1) {
				switch (sortBy) {
				case "Name":
					Collections.sort(itemCatalog, Item.Comparators.NAME);
					break;
				case "Price":
					Collections.sort(itemCatalog, Item.Comparators.PRICE);
					break;
				case "Date":
					Collections.sort(itemCatalog, Item.Comparators.ADD_DATE);
					break;
				default:
					request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
					return Path.PAGE__ERROR_PAGE;
					
				}
				if (reverse != null) {
					Collections.reverse(itemCatalog);
				}else {
					String temp = "sortedBy"+sortBy;
					request.setAttribute(temp, "true");
				}
				request.getSession().setAttribute("itemCatalog", itemCatalog);
			}
		}
		return Path.PAGE__CATALOG;
	}

}
