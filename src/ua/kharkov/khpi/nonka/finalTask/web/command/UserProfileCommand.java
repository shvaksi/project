package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.Role;
import ua.kharkov.khpi.nonka.finalTask.db.dao.OrderDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class UserProfileCommand extends Command{

	private static final long serialVersionUID = -4500890863092205620L;
	private static final Logger log = Logger.getLogger(UserProfileCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");
		HttpSession session = request.getSession();
		
		String forward = Path.PAGE__ERROR_PAGE;
		String targetUserProfile = request.getParameter("targ-user");
		User user = null;
		if(targetUserProfile != null && !targetUserProfile.isEmpty()) {
			UserDao ud = new UserDao();			
			user = ud.findUser(Long.valueOf(targetUserProfile));
			System.out.println("Target user fname --> "+user.getfName());
			if(user != null) {				
				request.setAttribute("targUser", user);
				forward = Path.PAGE__USER_PROFILE;
			}
		} else {
			user = (User) session.getAttribute("user");
			if(Role.getRole(user) == Role.ADMIN) {
				List<User> usersList = new UserDao().getAllUsers();
				request.setAttribute("AllOrders", new OrderDao().allOrders());
		    	if(usersList.size() > 0) {
		    		request.setAttribute("UsersList", usersList);    		
		    	}else {
		    		log.warn("Users is empty");
		    	}
			}
			request.setAttribute("userOrders", new OrderDao().findOrdersByClient(user));
			request.setAttribute("targUser", user);
			forward = Path.PAGE__USER_PROFILE;
		}
		return forward;
	}
	
}
