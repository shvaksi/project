package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class BanChangeCommand extends Command {
	private static final long serialVersionUID = 1109089477636569445L;
	private static final Logger log = Logger.getLogger(BanChangeCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long id = null;
		try {
			id = Long.valueOf(request.getParameter("targ-user"));
		} catch (Exception e) {
			request.setAttribute("errorMessage", "Invalid parameter");
			return Path.PAGE__ERROR_PAGE;
		}
		UserDao ud = new UserDao();
		User user = ud.findUser(id);
		if (user != null) {
			Map<Long, User> userChangingMap = (Map<Long, User>) request.getSession().getServletContext().getAttribute("userChangingMap");
			if (userChangingMap == null) {
				userChangingMap = new HashMap<Long, User>();
				log.debug("CREATE NEW userChangingMap");
			}
			if (request.getParameter("unBan") != null) {
				if (!user.getIsBanned()) {
					request.setAttribute("errorMessage", "User " + user.getEmail() + " is not banned");
					return Path.PAGE__ERROR_PAGE;
				} else {
					user.setBanned(false);
					ud.updateUserBan(user);				
					userChangingMap.put(user.getId(), user);
					request.getSession().getServletContext().setAttribute("userChangingMap", userChangingMap);
					request.setAttribute("redirect", "true");
					return Path.COMMAND__USER_PROFILE;
				}
			} else {
				if (user.getIsBanned()) {
					request.setAttribute("errorMessage", "User " + user.getEmail() + " is already banned");
					return Path.PAGE__ERROR_PAGE;
				} else {
					user.setBanned(true);
					ud.updateUserBan(user);
					if(userChangingMap == null || userChangingMap.isEmpty()) {
						userChangingMap = new HashMap<Long,User>();
					}
					userChangingMap.put(user.getId(), user);
					request.getSession().getServletContext().setAttribute("userChangingMap", userChangingMap);
					request.setAttribute("redirect", "true");
					return Path.COMMAND__USER_PROFILE;
				}
			}
		} else {
			request.setAttribute("message", "Invalid user id");
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
