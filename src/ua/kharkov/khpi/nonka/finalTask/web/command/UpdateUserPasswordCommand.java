package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class UpdateUserPasswordCommand extends Command {

	private static final long serialVersionUID = 3281069910313219710L;
	private static final Logger log = Logger.getLogger(UpdateUserInfoCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");
		String forward = Path.COMMAND__USER_PROFILE;
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		String error_upadates_password = Support.validatePasswords(request.getParameter("password"),
				request.getParameter("confirm_password"), FieldTemplate.TEMPLATE__USER_PASSWORD, bundle);

		if (error_upadates_password == null || error_upadates_password.isEmpty()) {
			User user = new User();
			User curUser = (User) request.getSession().getAttribute("user");
			user.setPassword(request.getParameter("password"));
			user.setId(curUser.getId());
			UserDao ud = new UserDao();
			ud.updateUserPassword(user);
		} else {
			request.getSession().setAttribute("error_upadates_password", error_upadates_password);
		}
		
		return forward;
	}

}
