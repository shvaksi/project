package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.CommentDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Comment;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class DeleteCommentCommand extends Command{
	private static final long serialVersionUID = -6212978098231282009L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		String forward = request.getHeader("Referer");
		request.setAttribute("redirect", "true");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Long targComment = null;
		User user = (User) request.getSession().getAttribute("user");
		try {
			targComment = Long.valueOf(request.getParameter("targ-comment"));
			CommentDao comDao = new CommentDao();
			Comment com = comDao.findById(targComment);
			if(com == null || (user.getRoleId() != 2 && user.getId() != com.getOwner().getId())) {
				throw new Exception("You can not delete not your comment");
			}
			comDao.deleteComment(com);
			return forward;
		}catch(Exception e) {
			request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
