package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.Role;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketPartDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Basket;
import ua.kharkov.khpi.nonka.finalTask.db.entity.BasketPart;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class LoginCommand extends Command {
	private static final long serialVersionUID = 6809790172222146716L;

	private static final Logger log = Logger.getLogger(LoginCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");

		HttpSession session = request.getSession();

		String def_locale = (String) session.getAttribute("defaultLocale");

		if (def_locale == null) {
			def_locale = "ru";
		}

		Locale.setDefault(new Locale(def_locale));
		ResourceBundle myBundle = ResourceBundle.getBundle("resources");
		// obtain email and password from the request
		String email = request.getParameter("email");
		log.trace("Request parameter: loging --> " + email);

		String password = request.getParameter("password");

		// error handler
		String loginErrorMessage = null;
		String forward = Path.PAGE__LOGIN;

		if (email == null || password == null || email.isEmpty() || password.isEmpty()) {
			loginErrorMessage = myBundle.getString("Login.Fields.Empty");
			session.setAttribute("loginErrorMessage", loginErrorMessage);
			log.error("loginErrorMessage --> " + loginErrorMessage);
			return forward;
		}

		User user = new UserDao().findUserByEmail(email);
		log.trace("Found in DB: user --> " + user);

		if (user == null || !Support.matching(user.getPassword(), password)) {
			loginErrorMessage = myBundle.getString("Login.Fields.Incorrect");
			session.setAttribute("loginErrorMessage", loginErrorMessage);
			log.error("errorMessage --> " + loginErrorMessage);
			return forward;
		} else {
			Role userRole = Role.getRole(user);
			log.trace("userRole --> " + userRole);

			if (userRole == Role.ADMIN) {
				log.debug("USER IS ADMIN");
			}

			if (userRole == Role.CLIENT)
				log.debug("USER IS	CLIENT");
			forward = Path.COMMAND__USER_PROFILE;

			session.setAttribute("user", user);
			log.trace("Set the session attribute: user --> " + user);

			session.setAttribute("userRole", userRole);
			log.trace("Set the session attribute: userRole --> " + userRole);
			BasketDao bd = new BasketDao();
			BasketPartDao bpd = new BasketPartDao();
			Basket basket = bd.findBasketByOwner(user);
			if (basket != null) {
				basket.setBasketParts(bpd.findByBasketId(basket.getId()));
				int totalPrice = 0;
				for (BasketPart bp : basket.getBasketParts()) {
					totalPrice += bp.getTotalPrice();
				}
				basket.setTotalPrice(totalPrice);
			}
			session.setAttribute("userBasket", basket);
			log.info("User " + user + " logged as " + userRole.toString().toLowerCase());

			// work with i18n
			String userLocaleName = user.getUserLocale();
			log.trace("userLocalName --> " + userLocaleName);
			Config.set(request.getSession(), "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);
			if (userLocaleName != null && !userLocaleName.isEmpty()) {
				Config.set(session, "javax.servlet.jsp.jstl.fmt.locale", userLocaleName);

				session.setAttribute("defaultLocale", userLocaleName);
				log.trace("Set the session attribute: defaultLocaleName --> " + userLocaleName);

				log.info("Locale for user: defaultLocale --> " + userLocaleName);
			}
		}

		log.debug("Command finished");
		return forward;

	}

}
