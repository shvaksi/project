package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.OrderDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.OrderPartDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Basket;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Order;
import ua.kharkov.khpi.nonka.finalTask.db.entity.OrderPart;

/**
 * @author D. Nonka
 *
 */
public class AddOrderCommand extends Command{
	private static final long serialVersionUID = 6585807475274014823L;
	private static final Logger log = Logger.getLogger(AddOrderCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start...");
		String forward = Path.COMMAND__USER_PROFILE;
		request.setAttribute("redirect", "true");
		Basket basket = null;
		HttpSession session = request.getSession();
		try {
			basket = (Basket) session.getAttribute("userBasket");
			if(basket == null || basket.getBasketParts().isEmpty()) {
				throw new Exception("Your basket is empty");
			}
		}catch(Exception e) {
			request.removeAttribute("redirect");
			request.setAttribute("errorMessage", e.getMessage());
			log.debug("Invalid Parameters");
			return Path.PAGE__ERROR_PAGE;
		}
		Order order = new Order(basket);
		log.debug("order created");
		order.setOrderDate(new Date());
		OrderDao orderDao = new OrderDao();
		orderDao.insertOrder(order);
		OrderPartDao opd = new OrderPartDao();
		for (OrderPart part : order.getOrderParts()) {
			part.setOrderId(order.getId().intValue());
			opd.insertOrderPart(part);
		}
		BasketDao basketDao = new BasketDao();
		basketDao.deleteBasket(basket);
		log.debug("Order added");
		session.setAttribute("userBasket", null);
		return forward;
	}

}
