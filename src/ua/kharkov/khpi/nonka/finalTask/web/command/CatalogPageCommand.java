package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;

/**
 * @author D. Nonka
 *
 */
public class CatalogPageCommand extends Command {
	private static final long serialVersionUID = -5984779739163769983L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long time = System.nanoTime();
		request.getSession().removeAttribute("appliedCategoryFilters");
		request.getSession().removeAttribute("appliedCountryFilters");
		request.getSession().removeAttribute("maxPrice");
		request.getSession().removeAttribute("minPrice");
		request.getSession().removeAttribute("searchingCategory");
		request.getSession().removeAttribute("searchByName");
		request.getSession().removeAttribute("itemCatalog");
		System.out.println((System.nanoTime() - time) / 1000000);
		ItemDao itemDao = new ItemDao();
		List<Item> allItems = itemDao.getAllItems();
		request.getSession().setAttribute("itemCatalog", allItems);
		return Path.PAGE__CATALOG;
	}

}
