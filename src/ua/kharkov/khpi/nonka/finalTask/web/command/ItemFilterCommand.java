package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemPropertyDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.ItemProperty;

/**
 * @author D. Nonka
 *
 */
public class ItemFilterCommand extends Command {
	private static final long serialVersionUID = 1485779525370777583L;
	private static final Logger log = Logger.getLogger(ItemFilterCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start");
		request.getSession().removeAttribute("appliedCategoryFilters");
		request.getSession().removeAttribute("appliedCountryFilters");
		request.getSession().removeAttribute("maxPrice");
		request.getSession().removeAttribute("minPrice");
		request.getSession().removeAttribute("searchingCategory");
		request.getSession().removeAttribute("searchByName");
		request.getSession().removeAttribute("itemCatalog");

		request.removeAttribute("redirect");
		StringBuilder querry = new StringBuilder("SELECT * FROM items WHERE");
		ItemPropertyDao itemPropDao = new ItemPropertyDao();
		List<ItemProperty> allCategory = itemPropDao.getAllCategories();
		List<ItemProperty> allCountries = itemPropDao.getAllCountries();
		int condCount = 0;
		Integer[] appliedCategoryFilters = new Integer[allCategory.size()];
		for (ItemProperty it : allCategory) {
			String res = request.getParameter(it.getName());
			if (res != null && res.equals("on")) {
				String querryPart = (condCount == 0) ? Fields.ITEM__CATEGORYID + " = " + it.getId()
						: " OR " + Fields.ITEM__CATEGORYID + " = " + it.getId();
				querry.append(" " + querryPart);
				appliedCategoryFilters[it.getId().intValue() - 1] = it.getId().intValue() - 1;
				condCount++;
			}
		}
		condCount = 0;
		String [] test = querry.toString().split(" ");
		if(!test[test.length-1].equals("WHERE") && !test[test.length-1].equals("AND")) {
			querry.append(" AND");
		}
		Integer[] appliedCountryFilters = new Integer[allCountries.size()];
		for (ItemProperty it : allCountries) {
			String res = request.getParameter(it.getName());
			if (res != null && res.equals("on")) {
				String querryPart = (condCount == 0) ? Fields.ITEM__COUNTRYID + " = " + it.getId()
						: " OR " + Fields.ITEM__COUNTRYID + " = " + it.getId();
				querry.append(" " + querryPart);
				appliedCountryFilters[it.getId().intValue() - 1] = it.getId().intValue() - 1;
				condCount++;
			}
		}
		test = querry.toString().split(" ");
		if(!test[test.length-1].equals("WHERE") && !test[test.length-1].equals("AND")) {
			querry.append(" AND");
		}
		request.getSession().setAttribute("appliedCategoryFilters", appliedCategoryFilters);
		request.getSession().setAttribute("appliedCountryFilters", appliedCountryFilters);
		Integer minPrice = null;
		Integer maxPrice = null;
		try {
			minPrice = Integer.valueOf(request.getParameter("min-price"));
			if (minPrice < 0) {
				minPrice = 0;
			}
		} catch (Exception e) {
			log.debug("MinPrice --> Incorrect");
			minPrice = 0;
		}
		request.getSession().setAttribute("minPrice", minPrice);
		try {
			maxPrice = Integer.valueOf(request.getParameter("max-price"));
		} catch (Exception e) {
			log.debug("MaxPrice --> Incorrect");
		}
		if (maxPrice != null && minPrice < maxPrice) {
			querry.append(" " + Fields.ITEM__PRICE + " BETWEEN " + minPrice + " AND " + maxPrice);
			request.getSession().setAttribute("maxPrice", maxPrice);
		} else {
			querry.append(" " + Fields.ITEM__PRICE + " > " + minPrice);
		}
		log.debug("Final querry --> " + querry.toString());
		ItemDao itemDao = new ItemDao();
		request.getSession().setAttribute("itemCatalog", itemDao.searchWithCustomQuerry(querry.toString()));
		return Path.PAGE__CATALOG;
	}

}
