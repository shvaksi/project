package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;

/**
 * @author D. Nonka
 *
 */
public class CancelUpdateUserCommand extends Command{

	private static final long serialVersionUID = -6953377869204021933L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		request.getSession().removeAttribute("error_upadates");
		request.getSession().removeAttribute("user_update");
		request.getSession().removeAttribute("error_upadates_password");
		return Path.COMMAND__USER_PROFILE;
	}

}
