package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemPropertyDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class AddItemCommand extends Command {
	private static final long serialVersionUID = 615079639535518130L;
	private static final Logger log = Logger.getLogger(AddItemCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");

		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		ItemDao itemDao = new ItemDao();

		Map<String, String> error_map = new HashMap<String, String>();
		for (int i = 0; i < 6; i++) {
			error_map.put(String.valueOf(i), null);
		}
		String forward = Path.PAGE__ITEM_CREATE;
		Item item = new Item();
		ItemPropertyDao ipd = new ItemPropertyDao();

		String item_name = Support.partToString(request.getPart("item-name"));

		if (itemDao.findByName(item_name) != null) {
			error_map.put(String.valueOf(0), bundle.getString("AddItem.Name.AlreadyExist"));
		} else {
			error_map.put(String.valueOf(0), Support.validateText(Support.partToString(request.getPart("item-name")),
					FieldTemplate.TEMPLATE__ITEM_NAME, bundle, "Registry.Fields.Incorrect"));
		}

		Integer price = null;
		try {
			price = Integer.valueOf(Support.partToString(request.getPart("item-price")));
		} catch (NumberFormatException e) {
			price = null;

		}

		if (price != null) {
			if (price <= 0 || price > 10000) {
				error_map.put(String.valueOf(1), bundle.getString("AddItem.Price.Incorrect"));
				log.debug("Price --> Incorrect");
			}
		} else {
			error_map.put(String.valueOf(1), bundle.getString("Field.Required.Empty"));
			log.debug("Price --> Empty");
		}
		String description = Support.partToString(request.getPart("item-description"));
		System.out.println("description string " + description);
		if (description == null || description.isEmpty()) {
			error_map.put(String.valueOf(4), bundle.getString("Field.Required.Empty"));
			log.debug("Add to error_map (4): " + bundle.getString("Field.Required.Empty"));
			log.debug("Description --> " + description);
		} else {
			if (description.length() > 4000) {
				error_map.put(String.valueOf(4), bundle.getString("AddItem.Description.Incorrect"));
				log.debug("Description --> Incorrect");
			}
		}

		Long category_id = Long.valueOf(Support.partToString(request.getPart("item-category")));
		if (category_id != null & ipd.findCategory(category_id) != null) {
			item.setCategory_id(category_id.intValue());
		} else {
			error_map.put(String.valueOf(2), bundle.getString("AddItem.Select.Incorrect"));
			log.debug("Category --> Incorrect");
		}

		Long country_id = Long.valueOf(Support.partToString(request.getPart("manufacturer")));
		if (country_id != null & ipd.findCountry(country_id) != null) {
			item.setCountry_id(country_id.intValue());
		} else {
			error_map.put(String.valueOf(3), bundle.getString("AddItem.Select.Incorrect"));
			log.debug("Category --> Incorrect");
		}

		for (int i = 0; i < error_map.size(); i++) {
			if (error_map.get(String.valueOf(i)) == null) {
				continue;
			} else {
				request.getSession().setAttribute("addItemErrors", error_map);
				return forward;
			}
		}

		String avatar = null;
		if (request.getPart("item-pict").getSize() > 0) {
			item.setName(request.getParameter("item-name"));
			log.debug("Image [first step] --> pass");
			log.debug("Expected item avatar name --> " + item.getName().trim().replaceAll("\\s+", "-"));
			try {
				avatar = Support.uploadFile(request, request.getPart("item-pict"), "/items/");
			} catch (NullPointerException e) {
				e.printStackTrace();
				System.out.println("Fail in command!");
			}
			log.debug("Actual item avatar name --> " + avatar);
			if (avatar != null) {
				item.setAvatar(avatar);
			} else {
				error_map.put(String.valueOf(5), bundle.getString("Image.Incorrect"));
				log.debug("Image --> Incorrect");
				return forward;
			}
		}

		item.setAddDate(new Date());
		item.setName(item_name);
		item.setDescription(description);
		item.setPrice(price);
		itemDao.insertItem(item);
		item = itemDao.findByName(item.getName());
		forward = Path.COMMAND__ITEM_PAGE + "&targ-item=" + item.getId();
		return forward;
	}

}
