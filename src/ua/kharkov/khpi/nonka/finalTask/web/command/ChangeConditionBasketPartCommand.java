package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.BasketPartDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Basket;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * @author D. Nonka
 *
 */
public class ChangeConditionBasketPartCommand extends Command{
	private static final long serialVersionUID = -5968498909822631338L;
	private static final Logger log = Logger.getLogger(ChangeConditionBasketPartCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start");
		String forward = request.getHeader("Referer");
		request.setAttribute("redirect", "true");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		User user = (User) request.getSession().getAttribute("user");
		Basket basket = null;
		Long targ_item = null;
		String action = null;
		try {			
			targ_item = Long.valueOf(request.getParameter("targ-item"));
			action = request.getParameter("action");
			basket = (Basket) request.getSession().getAttribute("userBasket");
			if(targ_item == null || action == null || basket == null) {
				throw new Exception("Null parameter");
			}
			BasketDao bd = null;
			if(user != null) {
				bd = new BasketDao();
			}
			switch(action) {
			case "plusOne":
				log.debug("Plus one");
				if(basket.plusCount(targ_item) && user != null) {
					bd.updateBasket(basket);
				}
				break;
			case "minusOne":
				log.debug("Minus one");
				if(basket.minusCount(targ_item) && user !=null) {
					bd.updateBasket(basket);
				}
				break;
			case "remove":
				Long res = basket.remove(targ_item);
				if(res != null && user != null) {
					BasketPartDao bpd = new BasketPartDao();
					bpd.deleteBasketPart(res);
				}
				break;
			default: throw new Exception("Invalid Parameter");
			}
			request.getSession().setAttribute("userBasket", basket);
			return forward;
		}
		catch(Exception e) {
			request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
