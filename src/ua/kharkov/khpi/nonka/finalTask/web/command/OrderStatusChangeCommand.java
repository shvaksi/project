package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.OrderDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Order;

/**
 * @author D. Nonka
 *
 */
public class OrderStatusChangeCommand extends Command{
	private static final long serialVersionUID = -3745879923964328794L;
	private static final Logger log = Logger.getLogger(OrderStatusChangeCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start...");
		request.setAttribute("redirect", "true");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Long targOrder = null;
		String action = request.getParameter("action");
		Order order = null;
		try {
			targOrder = Long.valueOf(request.getParameter("target-order"));
			OrderDao orderDao = new OrderDao();
			order = orderDao.findOrderById(targOrder);
			if(order == null) {
				throw new Exception("Incorrect order id");
			}
			if(!order.getStatus().equals("Registred")) {
				throw new Exception("Order already status chnged --> "+order.getStatus()); 
			}
			switch(action) {
				case "Pay":
					order.setStatus("Payed");
					break;
				case "Abort":
					order.setStatus("Aborted");
					break;
				default:
					throw new Exception("Unknown action");
			}
			orderDao.updateStatus(order);
		}catch(Exception e) {
			request.setAttribute("errorMessage", bundle.getString("Page.Invalid.Parameters"));
			log.debug("Invalid Parameters");
			return Path.PAGE__ERROR_PAGE;
		}
		return Path.COMMAND__USER_PROFILE;
	}
	
}
