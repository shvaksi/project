package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.CarouselItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.CarouselItem;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class AddCarouselItemCommand extends Command {
	private static final long serialVersionUID = 6254744570607799423L;
	private static final Logger log = Logger.getLogger(AddCarouselItemCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		request.getSession().removeAttribute("carouselImageError");
		request.getSession().removeAttribute("carouselHeaderError");
		request.getSession().removeAttribute("carouselNameError");
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		String forward = Path.COMMAND__USER_PROFILE;
		CarouselItem cIt = new CarouselItem();
		CarouselItemDao icd = new CarouselItemDao();

		String name = Support.partToString(request.getPart("carousel-name"));
		if (name != null) {
			String error = Support.validateText(name, FieldTemplate.TEMPLATE__ITEM_NAME, bundle,
					"Registry.Fields.Incorrect");
			if (error == null) {
				cIt.setName(name);
			} else {
				request.getSession().setAttribute("carouselNameError", error);
				return forward;
			}
		} else {
			request.getSession().setAttribute("carouselNameError", bundle.getString("Field.Required.Empty"));
			return forward;
		}

		String header = Support.partToString(request.getPart("carousel-info"));
		if (header != null) {
			String error = Support.validateText(header, FieldTemplate.TEMPLATE__ITEM_NAME, bundle,
					"Registry.Fields.Incorrect");
			if (error == null) {
				cIt.setHeader(header);
			} else {
				request.getSession().setAttribute("carouselHeaderError", error);
				return forward;
			}
		} else {
			request.getSession().setAttribute("carouselHeaderError", bundle.getString("Field.Required.Empty"));
			return forward;
		}

		if (request.getPart("carousel-img").getSize() > 0) {
			String avatar = null;
			
			try {
				avatar = Support.uploadFile(request, request.getPart("carousel-img"), "/carousel/");
				cIt.setImage(avatar);
			} catch (NullPointerException e) {
				e.printStackTrace();
				log.debug("Fail in command!");
				return forward;
			}
		} else {
			request.getSession().setAttribute("carouselImageError", bundle.getString("Image.Incorrect"));
			return forward;
		}
		icd.addCarouselItem(cIt);
		return Path.PAGE__INDEX;
	}

}
