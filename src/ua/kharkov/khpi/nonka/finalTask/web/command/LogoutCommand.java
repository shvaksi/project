package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;

/**
 * @author D. Nonka
 *
 */
public class LogoutCommand extends Command{
	private static final long serialVersionUID = -2352369606030659629L;

	private static final Logger log = Logger.getLogger(LogoutCommand.class);
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command starts");
		request.setAttribute("redirect", "true");
		HttpSession session = request.getSession(false);
		if (session != null)
			session.invalidate();
		
		log.debug("Command finished");
		return Path.PAGE__INDEX;

	}

}
