package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.CarouselItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.CarouselItem;

/**
 * @author D. Nonka
 *
 */
public class RemoveCarouselItem extends Command{
	private static final long serialVersionUID = 6836411108023558003L;

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		Long id = null;
		try {
			id= Long.valueOf(request.getParameter("targ-item"));
		}
		catch(Exception e) {
			request.setAttribute("errorMessage", "Invalide parameter");
			return Path.PAGE__ERROR_PAGE;
		}
		CarouselItemDao cItDao = new CarouselItemDao();
		CarouselItem cIt = cItDao.findById(id);
		if(cIt != null) {
			cItDao.removeCarouselItem(cIt);
			return Path.PAGE__INDEX;
		}else {
			request.setAttribute("errorMessage", "Invalid item id");
			return Path.PAGE__ERROR_PAGE;
		}
	}

}
