package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class UpdateItemImageCommand extends Command{
	private static final long serialVersionUID = -4452449793337133289L;
	private static final Logger log = Logger.getLogger(UpdateItemImageCommand.class);
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		String forward = Path.PAGE__ERROR_PAGE;
		Long id = null;
		Item item = null;
		ItemDao itemDao = new ItemDao();
		try {
			id = Long.valueOf(Support.partToString(request.getPart("targ-item")));
			item = itemDao.findById(id);			
		}
		catch(NullPointerException e){
			e.printStackTrace();
		}
		if (item == null) {
			request.getSession().setAttribute("message", "Unknown item with id "+request.getParameter("targ-item"));
			return forward;
		}
		String avatar = null;
		try {
			avatar = Support.uploadFile(request, request.getPart("item-pict"), "/items/");
		} catch (NullPointerException e) {
			e.printStackTrace();
			log.debug("Fail in command!");
		}
		log.debug("Actual item avatar name --> " + avatar);
		if (avatar != null) {
			item.setAvatar(avatar);
		} else {
			String error_upadates_avatar = bundle.getString("AddItem.Image.Incorrect");
			request.getSession().setAttribute("error_upadates_avatar", error_upadates_avatar);
			log.debug("Image --> Incorrect");
			return forward;
		}
		itemDao.updateItemImage(item);
		return Path.PAGE__INDEX;
	}

}
