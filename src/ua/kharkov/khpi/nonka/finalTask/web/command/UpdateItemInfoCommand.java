package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemPropertyDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.Item;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;
/**
 * @author D. Nonka
 *
 */
public class UpdateItemInfoCommand extends Command {
	private static final long serialVersionUID = 8799208463854621767L;
	private static final Logger log = Logger.getLogger(UpdateItemInfoCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		Map<String, String> error_map = new HashMap<String, String>();
		String forward = Path.PAGE__ITEM_CREATE;
		Item item = new Item();
		ItemPropertyDao ipd = new ItemPropertyDao();
		ItemDao itemDao = new ItemDao();
		Item targItem = null;
		if ((request.getParameter("targ-item") == null
				|| (targItem = itemDao.findById(Long.valueOf(request.getParameter("targ-item")))) == null)) {
			String message = bundle.getString("Page.Invalid.Parameters");
			request.setAttribute("message", message);
			return Path.PAGE__ERROR_PAGE;
		}
		if (itemDao.findByName(request.getParameter("item-name")) != null) {
			error_map.put(String.valueOf(0), bundle.getString("AddItem.Name.AlreadyExist"));
		} else {
			error_map.put(String.valueOf(0), Support.validateText(request.getParameter("item-name"),
					FieldTemplate.TEMPLATE__ITEM_NAME, bundle, "Registry.Fields.Incorrect"));
		}
		Integer price = null;

		try {
			price = Integer.valueOf(request.getParameter("item-price"));
		} catch (NumberFormatException e) {
			price = null;

		}

		if (price != null) {
			if (price <= 0 || price > 10000) {
				error_map.put(String.valueOf(1), bundle.getString("AddItem.Price.Incorrect"));
				log.debug("Price --> Incorrect");
			}
		} else {
			error_map.put(String.valueOf(1), bundle.getString("Field.Required.Empty"));
			log.debug("Price --> Empty");
		}
		String description = request.getParameter("item-description");
		if (description != null) {
			if (description.length() > 4000) {
				error_map.put(String.valueOf(4), bundle.getString("AddItem.Description.Incorrect"));
				log.debug("Description --> Incorrect");
			}
		} else {
			error_map.put(String.valueOf(4), bundle.getString("Field.Required.Empty"));
			log.debug("Description --> Empty");
		}

		Long category_id = Long.valueOf(request.getParameter("item-category"));
		if (category_id != null & ipd.findCategory(category_id) != null) {
			item.setCategory_id(category_id.intValue());
		} else {
			error_map.put(String.valueOf(2), bundle.getString("AddItem.Select.Incorrect"));
			log.debug("Category --> Incorrect");
		}

		Long country_id = Long.valueOf(request.getParameter("manufacturer"));
		if (country_id != null & ipd.findCountry(country_id) != null) {
			item.setCountry_id(country_id.intValue());
		} else {
			error_map.put(String.valueOf(3), bundle.getString("AddItem.Select.Incorrect"));
			log.debug("Category --> Incorrect");
		}

		for (int i = 0; i < error_map.size(); i++) {
			if (error_map.get(String.valueOf(i)) != null) {
				request.getSession().setAttribute("updateItemInfoErrors", error_map);
				return forward;
			}
		}
		targItem.setName(request.getParameter("item-name"));
		targItem.setPrice(price);
		targItem.setDescription(description);
		targItem.setCategory_id(Integer.parseInt(request.getParameter("item-category")));
		targItem.setCountry_id(Integer.parseInt(request.getParameter("manufacturer")));
		itemDao.updateItemInfo(targItem);
		forward = Path.COMMAND__ITEM_PAGE + "&targ-item=" + targItem.getId();
		return forward;
	}

}
