package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.Fields;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemDao;
import ua.kharkov.khpi.nonka.finalTask.db.dao.ItemPropertyDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.ItemProperty;

/**
 * @author D. Nonka
 *
 */
public class SearchItemCommand extends Command {
	private static final long serialVersionUID = 447751129350140738L;
	private static final Logger log = Logger.getLogger(SearchItemCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		log.debug("Command start");
		
		request.getSession().removeAttribute("appliedCategoryFilters");
		request.getSession().removeAttribute("appliedCountryFilters");
		request.getSession().removeAttribute("maxPrice");
		request.getSession().removeAttribute("minPrice");
		request.getSession().removeAttribute("searchingCategory");
		request.getSession().removeAttribute("searchByName");
		request.getSession().removeAttribute("itemCatalog");
		
		Integer category = null;
		request.removeAttribute("redirect");
		try {
			category = Integer.valueOf(request.getParameter("searchingCategory"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (category == null) {
			request.setAttribute("errorMessage", "Incorect category");
			return Path.PAGE__ERROR_PAGE;
		}
		ItemDao itemDao = new ItemDao();
		ItemPropertyDao itPropDao = new ItemPropertyDao();
		StringBuilder querry = new StringBuilder("SELECT * FROM items");
		String searchByName = request.getParameter("searchByName");

		if (category == -1) {
			log.debug("All Categories");
			querry.append(" WHERE");
			ItemProperty allCategories = new ItemProperty();
			allCategories.setName("All Categories");
			request.getSession().setAttribute("searchingCategory", allCategories);
		} else if (itPropDao.findCategory(category.longValue()) != null) {
			querry.append(" WHERE " + Fields.ITEM__CATEGORYID + " = " + category);
			request.getSession().setAttribute("searchingCategory", itPropDao.findCategory(category.longValue()));
		} else {
			request.setAttribute("errorMessage", "Icorrect category id");
			return Path.PAGE__ERROR_PAGE;
		}
		if (searchByName != null && !searchByName.isEmpty()) {
			String querryPart = (category == -1) ? " " : " AND ";
			request.getSession().setAttribute("searchByName", searchByName);
			querry.append(querryPart + Fields.ITEM__NAME + " LIKE \"%" + searchByName + "%\"");
		}
		log.debug("Final querry --> " + querry.toString());
		request.getSession().setAttribute("itemCatalog", itemDao.searchWithCustomQuerry(querry.toString()));
		return Path.PAGE__CATALOG;
	}

}
