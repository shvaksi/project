package ua.kharkov.khpi.nonka.finalTask.web.command;

import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.dao.UserDao;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

/**
 * @author D. Nonka
 *
 */
public class UpdateUserImageCommand extends Command {
	private static final long serialVersionUID = 7304131083006913512L;
	private static final Logger log = Logger.getLogger(UpdateUserImageCommand.class);

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		ResourceBundle bundle = ResourceBundle.getBundle("resources");
		String forward = Path.PAGE__ERROR_PAGE;
		User user = (User) request.getSession().getAttribute("user");
		if (user == null) {
			return forward;
		}
		String avatar = null;
		try {
			avatar = Support.uploadFile(request, request.getPart("user-avatar"), "/users/");
		} catch (NullPointerException e) {
			e.printStackTrace();
			log.debug("Fail in command!");
		}
		log.debug("Actual user avatar name --> " + avatar);
		if (avatar != null) {
			user.setAvatar(avatar);
		} else {
			String error_upadates_avatar = bundle.getString("AddItem.Image.Incorrect");
			request.getSession().setAttribute("error_upadates_avatar", error_upadates_avatar);
			log.debug("Image --> Incorrect");
			return forward;
		}

		UserDao ud = new UserDao();
		ud.updateUserAvatar(user);
		return Path.COMMAND__USER_PROFILE;
	}

}
