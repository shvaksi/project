package ua.kharkov.khpi.nonka.finalTask.web.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.kharkov.khpi.nonka.finalTask.Path;
import ua.kharkov.khpi.nonka.finalTask.db.Role;
import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

/**
 * 
 * @author D. Nonka
 *
 *         Servlet Filter implementation class CommandAccessFilter
 */
public class CommandAccessFilter implements Filter {
	private static final Logger log = Logger.getLogger(CommandAccessFilter.class);
	private static Map<Role, List<String>> accessMap = new HashMap<Role, List<String>>();
	private static List<String> commons = new ArrayList<String>();
	private static List<String> outOfControl = new ArrayList<String>();
	private static List<String> isAnon = new ArrayList<String>();
	private static List<String> isBanned = new ArrayList<String>();

	public void destroy() {
		// TODO Auto-generated method stub
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (accessAllowed(request)) {
			chain.doFilter(request, response);
		} else {
			String errorMessasge = "You do not have permission to access the requested resource";
			String errorPlus = (String) request.getAttribute("bannedMessage");
			request.setAttribute("errorMessage", errorPlus != null ? errorMessasge + errorPlus : errorMessasge);
			request.getRequestDispatcher(Path.PAGE__ERROR_PAGE).forward(request, response);

		}
	}

	private boolean accessAllowed(ServletRequest request) {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		String commandName = request.getParameter("command");
		log.debug(commandName);

		if (commandName == null || commandName.isEmpty()) {
			return false;
		}

		if (outOfControl.contains(commandName))
			return true;

		HttpSession session = httpRequest.getSession(false);
		if ((session == null || session.getAttribute("userRole") == null) && isAnon.contains(commandName)) {
			return true;
		}

		Role userRole = (Role) session.getAttribute("userRole");
		if (userRole == null) {
			return false;
		}

		User user = (User) session.getAttribute("user");
		try {
			Map<Long, User> userChangingMap = (Map<Long, User>) session.getServletContext().getAttribute("userChangingMap");

			if (userChangingMap.get(user.getId()) != null) {
				user = userChangingMap.get(user.getId());
				userChangingMap.remove(user.getId());
				session.setAttribute("userRole", Role.getRole(user));
				System.out.println("Now Role is " + Role.getRole(user));
				session.getServletContext().setAttribute("userChangingMap", userChangingMap);
				session.setAttribute("user", user);
			}
		} catch (NullPointerException e) {
			System.out.println("userChangingMap is empty");
		}

		if (user == null || (user.getIsBanned() && isBanned.contains(commandName))) {
			request.setAttribute("bannedMessage", " cause you are banned");
			return false;
		}
		return accessMap.get(userRole).contains(commandName) || commons.contains(commandName);
	}

	public void init(FilterConfig fConfig) throws ServletException {
		accessMap.put(Role.CLIENT, asList(fConfig.getInitParameter("client")));
		accessMap.put(Role.ADMIN, asList(fConfig.getInitParameter("admin")));
		commons = asList(fConfig.getInitParameter("common"));
		outOfControl = asList(fConfig.getInitParameter("out-of-control"));
		isAnon = asList(fConfig.getInitParameter("is-anon"));
		isBanned = asList(fConfig.getInitParameter("is-banned"));

	}

	private List<String> asList(String str) {
		List<String> list = new ArrayList<String>();
		StringTokenizer st = new StringTokenizer(str);
		while (st.hasMoreTokens())
			list.add(st.nextToken());
		return list;
	}

}
