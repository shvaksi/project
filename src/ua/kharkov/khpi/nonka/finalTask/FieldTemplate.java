package ua.kharkov.khpi.nonka.finalTask;

public final class FieldTemplate {
	public static final String TEMPLATE__USER_NAME = "^[A-ZА-Я]+[a-zа-я]{1,15}$";
	public static final String TEMPLATE__USER_EMAIL = "^[A-Z_\\-a-z0-9]{1,20}+@+[a-z]{1,}+\\.+[a-z]{1,}$";
	public static final String TEMPLATE__USER_GENDER = "^Male|Female$";
	public static final	String TEMPLATE__USER_NUMBER = "^0+[0-9]{9}$";
	public static final String TEMPLATE__USER_PASSWORD = "^.{5,15}$";
	
	public static final String TEMPLATE__ITEM_NAME = "^([A-Za-zА-Яа-яё0-9\\s]){3,40}$";
}
