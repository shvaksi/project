package ua.kharkov.khpi.nonka.finalTask.validator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestSupportPasswordValidate.class, TestSupportTextValidate.class, TestEntityUser.class})
public class AllTests {

}
