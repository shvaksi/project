package ua.kharkov.khpi.nonka.finalTask.validator;

import static org.junit.Assert.*;

import org.junit.Test;

import ua.kharkov.khpi.nonka.finalTask.db.entity.User;

public class TestEntityUser {

	@Test
	public void test() {
		User user = new User();
		user.setlName("Bandy");
		assertEquals("Bandy",user.getlName());
		user.setfName("John");
		assertEquals("John",user.getfName());
		user.setAvatar("someAvatar.jpg");
		assertEquals("someAvatar.jpg",user.getAvatar());
	}

}
