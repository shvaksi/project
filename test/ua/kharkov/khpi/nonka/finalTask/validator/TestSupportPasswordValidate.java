package ua.kharkov.khpi.nonka.finalTask.validator;

import static org.junit.Assert.*;

import java.util.ResourceBundle;

import org.junit.Test;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

public class TestSupportPasswordValidate {
	@Test
	public void test() {
		ResourceBundle myBundle = ResourceBundle.getBundle("resources");
		assertEquals(null,
				Support.validatePasswords("12345", "12345", FieldTemplate.TEMPLATE__USER_PASSWORD, myBundle));
		assertEquals(myBundle.getString("Field.Required.Empty"),
				Support.validatePasswords("", "12345", FieldTemplate.TEMPLATE__USER_PASSWORD, myBundle));
		assertEquals(myBundle.getString("Registry.Fields.Incorrect") + " " + FieldTemplate.TEMPLATE__USER_PASSWORD,
				Support.validatePasswords("123", "123", FieldTemplate.TEMPLATE__USER_PASSWORD, myBundle));
		assertEquals(myBundle.getString("Registry.Password.DoNotMacth"),
				Support.validatePasswords("12345", "123456", FieldTemplate.TEMPLATE__USER_PASSWORD, myBundle));
	
	}
}
