package ua.kharkov.khpi.nonka.finalTask.validator;

import static org.junit.Assert.*;

import java.util.ResourceBundle;

import org.junit.Test;

import ua.kharkov.khpi.nonka.finalTask.FieldTemplate;
import ua.kharkov.khpi.nonka.finalTask.web.validator.Support;

public class TestSupportTextValidate {

	@Test
	public void test() {
		ResourceBundle myBundle = ResourceBundle.getBundle("resources");
		assertEquals(myBundle.getString("Registry.Fields.Incorrect")+ " " + FieldTemplate.TEMPLATE__USER_NAME, Support.validateText("sdsdsd", FieldTemplate.TEMPLATE__USER_NAME, myBundle, "Registry.Fields.Incorrect"));
		assertEquals(null, Support.validateText("John", FieldTemplate.TEMPLATE__USER_NAME, myBundle, "Registry.Fields.Incorrect"));
		assertEquals(myBundle.getString("Field.Required.Empty"), Support.validateText("", FieldTemplate.TEMPLATE__USER_NAME, myBundle, "Registry.Fields.Incorrect"));

	}

}
