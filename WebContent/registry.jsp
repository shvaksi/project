<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

	<c:set var="title" value="Simple Name" />
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
<div class="container-fluid page-wrapper">
		<c:set var="company_name" value="My Company"></c:set>
		<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<div class="main-content">
		<div class="container">	
		<c:choose>	
		<c:when test="${ empty sessionScope.user}">	
			<div class="omb_login">				
			<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6" style="z-index: 0">
	<form class="omb_loginForm reg-form" action="controller" autocomplete="off" method="POST">
					<input type="hidden" name="command" value="registerUser">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="text" class="form-control" name="first_name" placeholder="First name" required>
					</div>
					<c:choose>
						
						<c:when test="${not empty requestScope.error_map['0']}">
							<span class="help-block">${error_map['0'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="text" class="form-control" name="last_name" placeholder="Last name" required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['1']}">
							<span class="help-block">${error_map['1'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group radio-group">						
						<div class="btn-group radio-group" data-toggle="buttons">
  							<label class="btn btn-default active">
  							  <input type="radio" name="gender" id="option1" autocomplete="off" value="Male" checked> Male
  							</label>
  							<label class="btn btn-default">
  							  <input type="radio" name="gender" id="option2" autocomplete="off" value="Female"> Female
  							</label>
  						</div>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['2']}">
							<span class="help-block">${error_map['2'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="date" class="form-control" name="birthday"  required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['3']}">
							<span class="help-block">${error_map['3'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
						<input type="email" class="form-control" name="email" placeholder="Email address" pattern="^[A-Z_\\-a-z0-9]{1,20}+@+[a-z]{1,}+\\.+[a-z]{1,}$" required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['4']}">
							<span class="help-block">${error_map['4'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-phone"></i></span>
						<input type="tel" class="form-control" name="phone_number" placeholder="0XXXXXXXXX" pattern="^0+[0-9]{9}$" required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['5']}">
							<span class="help-block">${error_map['5'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						<input type="password" class="form-control" name="password" placeholder="Password" pattern="^.{5,15}$" required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['6']}">
							<span class="help-block">${error_map['6'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						<input type="password" class="form-control" name="confirm_password" placeholder="Confirm password" pattern="^.{5,15}$" required>
					</div>
					<c:choose>
						<c:when test="${not empty error_map['6']}">
							<span class="help-block">${error_map['6'] }</span>
						</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>					
					</c:choose>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
				</form>
			</div>
    	</div>
    	<div class="row omb_row-sm-offset-3 omb_loginOr reg-form">
			<div class="col-xs-12 col-sm-6">
				<hr class="omb_hrOr">
				<span class="omb_spanOr"><a id="login-form-open" href="login.jsp">Login</a></span>
			</div>
		</div>
		</div>
		</c:when>
			<c:otherwise>
				<c:redirect url="controller?command=userProfile"></c:redirect> 
			</c:otherwise>
		</c:choose>
			</div>
		</div>
		</div>
</body>
</html>