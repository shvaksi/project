
function tableSearch(targInput,tableID) {
  // Declare variables 
  var input, targetTD, filter, table, tr, td, i;
  input =  document.getElementById(targInput);;
  filter = input.value.toUpperCase();
  table = document.getElementById(tableID);
  targetTD = $("select[name=option-for-search-"+tableID+"]").val();
  tr = table.getElementsByTagName("tr");

  // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[targetTD];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}
function tableSort(table, curTh){
	tr = table.find("tbody").find("tr");
	//var thead = curTh.parent();
	var td, rows, switching, i, x, y, shouldSwitch;
	switching = true;
	
	if(curTh.hasClass("searched")){
	while (switching) {
		curTh.removeClass("searched");
    	// Start by saying: no switching is done:
    	switching = false;
    	rows = table.find("tbody").find("tr");
    	/* Loop through all table rows (except the
    	first, which contains table headers): */
    	for (i = 0; i < (rows.length - 1); i++) {
    	  // Start by saying there should be no switching:
    	  shouldSwitch = false;
    	  /* Get the two elements you want to compare,
    	  one from current row and one from the next: */
    	  x = rows[i].getElementsByTagName("td")[curTh.index()];
    	  y = rows[i + 1].getElementsByTagName("td")[curTh.index()];
    	  // Check if the two rows should switch place:
    	  if (x.innerHTML.toUpperCase() < y.innerHTML.toUpperCase()) {
    	    // I so, mark as a switch and break the loop:
    	    shouldSwitch= true;
    	    break;
    	  }
    	}
    	if (shouldSwitch) {
    	  /* If a switch has been marked, make the switch
    	  and mark that a switch has been done: */
    	  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
    	  switching = true;
    	}
    }
  	}
  	else{
  		while (switching) {
		curTh.addClass("searched");
    	// Start by saying: no switching is done:
    	switching = false;
    	rows = table.find("tbody").find("tr");
    	/* Loop through all table rows (except the
    	first, which contains table headers): */
    	for (i = 0; i < (rows.length - 1); i++) {
    	  // Start by saying there should be no switching:
    	  shouldSwitch = false;
    	  /* Get the two elements you want to compare,
    	  one from current row and one from the next: */
    	  x = rows[i].getElementsByTagName("td")[curTh.index()];
    	  y = rows[i + 1].getElementsByTagName("td")[curTh.index()];
    	  // Check if the two rows should switch place:
    	  if (x.innerHTML.toUpperCase() > y.innerHTML.toUpperCase()) {
    	    // I so, mark as a switch and break the loop:
    	    shouldSwitch= true;
    	    break;
    	  }
    	}
    	if (shouldSwitch) {
    	  /* If a switch has been marked, make the switch
    	  and mark that a switch has been done: */
    	  rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
    	  switching = true;
    	}
    }
  	}
}
$(document).ready(function(){

	$('table > thead > tr > th').click(function(){	
		var curTh = $(this);
		var curTr = $(this).parent();
		var table = curTr.parent().parent();
		var length = curTr.find('th:last-child').index();
		if(curTh.index() < length){
			tableSort(table, curTh);
		}
	});
	$('.table-searching').keyup(function(e){
		var tableID = $(this).attr("target-table");
		var targInput = e.target.id;
		tableSearch(targInput, tableID);		
	});

	$('.tab-menu>ul.nav-tabs>li').click(function(){
		if(!($(this).hasClass("active"))){
			$(this).parent().find("li.active").removeClass("active");
			var target_slide = $(this).attr("data-open");
			$(this).addClass("active");
			$("div.tab-slides").find("div.active").removeClass("active");
			$("div.tab-slides").find("div#tab-slide-"+target_slide).addClass("active");
			return false;
		}
	});	
	$(".icon-down").click(function(){
	    if($(this).css("transform")=='none'){
	    	$(this).css("transform","rotate(180deg)");
	    }else{
	    	$(this).css("transform","");
	    }
		$(this).siblings(".option-block").slideToggle("slow");
		return false;
	});
	$(window).resize(function(){
		if($(window).width() >= 1023){
			$('.minicart').css({"right" : "-340px"});
			$('.minicart').css({"visibility":"visible"});
			return false;
		}
		else{
			$('.minicart').css({"right" : "-100%"});
			$('.minicart').css({"visibility":"hidden"});
			return false;
		}
	});
		$('.minicart-header').mouseenter(function(){
			if($(window).width() >= 1023){
				$('.minicart').addClass("active");
				$('.minicart').animate({right: '0px'}, "fast");
				$('.minicart').find('.minicart-title').find('.price').animate({opacity: '0'},"fast");
				$('.minicart').find('.minicart-opened-title-total-price').find('.price').animate({opacity: '1'},"fast");
				return false;
			}
		});
	
		$('.minicart').mouseleave(function(){
			if($(window).width() >= 1023){
				$('.minicart').removeClass("active");
				$('.minicart').animate({right:'-340px'}, "fast");
				$('.minicart').find('.minicart-title').find('.price').animate({opacity: '1'},"fast");		
				$('.minicart').find('.minicart-opened-title-total-price').find('.price').animate({opacity: '0'},"fast");
				return false;
			}
		});

	$("#mobile-cart-btn").click(function(){
		$('.minicart').css({"visibility":"visible"});
		$('.minicart').animate({right: '0px'}, "fast");
		return false;
	});

	$("#mobile-cart-back-btn").click(function(){		
		$('.minicart').animate({right: '-100%'}, "fast",function(){
			$('.minicart').css({"visibility":"hidden"});	
			return false;
		});
	});

	$("#mobile-navig-btn").click(function(){
		$('.menu-overlay').css({"display":"block"});
		$('.mobile-menu').animate({left:"0"},"fast");
		$('.mobile-menu').css({"visibility":"visible"});
		return false;
	});

	$('.menu-overlay').click(function(){
		$('.mobile-menu').animate({left:"-82.7%"},"fast", function(){
			$('.mobile-menu').css({"visibility":"hidden"});
		});
		$('.menu-overlay').css({"display":"none"});
		return false;
	});
});

