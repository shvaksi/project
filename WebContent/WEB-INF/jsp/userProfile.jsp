<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ taglib prefix = "my" uri = "/WEB-INF/myTags.tld" %>

<html>

<c:choose>
	<c:when test="${not empty sessionScope.user}">
		<c:set var="title" value="${sessionScope.user.fName }" />
	</c:when>
	<c:otherwise>
		<c:set var="title" value="We have some problem" />
	</c:otherwise>
</c:choose>

<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>

<c:if test="${sessionScope.userRole eq 'ADMIN'}">
	<div id="mini-form" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="controller" method="POST" enctype="multipart/form-data">	
				<input type="hidden" name="command" value="addCarouselItem" />
			 	<div class="modal-header">
      				<h5 class="modal-title">Add carousel item</h5>
      			 	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      			   		<span aria-hidden="true">&times;</span>
      			 	</button>
      			</div>      							
            	<div class="modal-body">
            		<div class="form-group">
            			<label for="carousel-img">Select image for carousel item</label>
            			<input type="file" name="carousel-img">
            			<c:if test="${not empty carouselImageError}"><p style="color:red;">${carouselImageError}</p></c:if> 
            		</div>
            		<div class="form-group">
            			<label for="carousel-info">Write name for carousel item</label>
            			<input type="text" class="form-control" name="carousel-name"></input>
            			<c:if test="${not empty carouselNameError}"><p style="color:red;">${carouselNameError}</p></c:if> 
            			
            		</div>
            		<div class="form-group">
            			<label for="carousel-info">Write information for carousel item</label>
            			<input type="text" class="form-control" name="carousel-info"></input>
            			<c:if test="${not empty carouselHeaderError}"><p style="color:red;">${carouselHeaderError}</p></c:if> 
            			
            		</div>
            	</div>
            	<div class="modal-footer">
        			<button class="btn btn-primary" type="submit">Add</button>
        			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      			</div>
      			</form>
            </div>
        </div>            				 	 	
	</div>
	</c:if>
	<div class="container-fluid page-wrapper">
		<c:set var="company_name" value="My Company"></c:set>
		<%@ include file="/WEB-INF/jspf/header.jspf" %>
		
		<div class="main-content">
			<div class="container-fluid">				 
				<div class="tabs">
				<c:if test="${sessionScope.user.email eq requestScope.targUser.email }">				
					<div class="tab-menu">
						<ul class="nav nav-tabs">
								<c:choose>
									<c:when test="${empty sessionScope.error_upadates }">
										<li data-open="1" class="active"><a href="#"><fmt:message key="UserProfile.label.Profile"/></a></li>
										<li data-open="2"><a href="#"><fmt:message key="UserProfile.label.MyOrders"/></a>
										<li data-open="3" ><a href="#"><fmt:message key="UserProfile.label.EditProfile"/></a></li>
									</c:when>
									<c:otherwise>
										<li data-open="1" ><a href="#"><fmt:message key="UserProfile.label.Profile"/></a></li>
										<li data-open="2"><a href="#"><fmt:message key="UserProfile.label.MyOrders"/></a>
										<li data-open="3" class="active"><a href="#"><fmt:message key="UserProfile.label.EditProfile"/></a></li>
									</c:otherwise>
								</c:choose>
								<c:if test="${sessionScope.user.roleId eq 2}">
								<li data-open="4" ><a href="#"><fmt:message key="UserProfile.label.AdminMenu"/></a></li>
								</c:if>							
						</ul>
					</div>
					</c:if>
					<div class="tab-slides">
						<!--PROFILE -->
						
						<c:choose>
							<c:when test="${requestScope.targUser.email ne sessionScope.user.email or empty sessionScope.error_upadates }">
								<div id="tab-slide-1" class="slide active">
							</c:when>
							<c:otherwise>
								<div id="tab-slide-1" class="slide">
							</c:otherwise>
						</c:choose>
							<div class="cols">
								<div class="left-col">
									<img class="img-responsive" src="${requestScope.targUser.avatar }">
								</div>
								<div class="right-col">
									<div class="name">
										<h1>${requestScope.targUser.fName} ${requestScope.targUser.lName} </h1>
										<c:if test="${requestScope.targUser.isBanned }">
											<p style="color:red;"><fmt:message key="UserProfile.label.Banned"/></p>
										</c:if>
									</div>
									<table class="table">
										<tr>
											<th><fmt:message key="UserProfile.label.Gender"/>:</th>
											<td><c:out value="${requestScope.targUser.gender}"/></td>
										</tr>
										<tr>
											<th><fmt:message key="UserProfile.label.DateOfBirth"/>:</th>
											<td>${requestScope.targUser.birthDate}</td>
										</tr>
										<tr>
											<th><fmt:message key="UserProfile.label.DateOfJoin"/>:</th>
											<td>${requestScope.targUser.joinDate}</td>
										</tr>
										<tr>
											<th><fmt:message key="UserProfile.label.Email"/>:</th>
											<td>${requestScope.targUser.email}</td>
										</tr>
										<tr>
											<th><fmt:message key="UserProfile.label.TelNumb"/>:</th>
											<td>${requestScope.targUser.telNumber}</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						
						<c:if test="${sessionScope.user.email eq requestScope.targUser.email }">
						<div id = "tab-slide-2" class="slide">
							<table class="table table-striped table-bordered table-list">
								<thead>
								<tr>
									<th>id</th>
									<th>Items</th>
									<th>Total Price</th>
									<th>Order Date</th>
									<th>Status</th>
								</tr>
								</thead>
								<tbody>
								<c:choose>
								<c:when test="${not empty  requestScope.userOrders}">
								<c:forEach items="${requestScope.userOrders}" var="order">
									<c:choose>
										<c:when test="${ order.status eq 'Payed'}">
											<tr class="success">
										</c:when>
										<c:when test="${order.status eq 'Aborted'}">
											<tr class="danger">
										</c:when>
										<c:otherwise>
											<tr class="info">
										</c:otherwise>	
									</c:choose>
									<td>${order.id }</td>
									<td>
									<c:forEach items="${order.orderParts}" var="orderPart">										
										<p>${orderPart.item.name} ( ${orderPart.item.price} x  ${orderPart.count } = ${orderPart.total_price })<p>
									</c:forEach>
									</td>
									<td>${order.totalPrice }</td>
									<td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${order.orderDate}" />
									<td>${order.status }</td>
									</tr>
								</c:forEach>
								</c:when>
								<c:otherwise>
									<tr>
									<td colspan = "5" align="center"><h3>Sorry, you have not orders</h3></td>
									</tr>
								</c:otherwise>
								</c:choose>
								</tbody>
							</table>
						</div>
						
						
						<!-- PROFILE EDIT -->
						<c:choose>
						<c:when test="${empty sessionScope.error_upadates }">
						<div id="tab-slide-3" class="slide">
						</c:when>
						<c:otherwise>
						<div id="tab-slide-3" class="slide active">
						</c:otherwise>
						</c:choose>
							<div class="cols">
								<div class="left-col">
									<img class="img-responsive" src="${sessionScope.user.avatar }">
									<form action="controller" method="POST" enctype="multipart/form-data">
									<input type="hidden" name="command" value="userAvatarUpdate">
									<div class="input-group">
										<input class="form-control" type="file" name="user-avatar" required>
										<c:if test="${not empty sessionScope.error_upadates_avatar}">
											<p style="color:red;">${sessionScope.error_upadates_avatar}</p>											
										</c:if>
									</div>
									<button class="btn btn-primary form-control" type="submit">Set new avatar</button>
									
									</form>
								</div>
								<div class="right-col">									
									<table class="table">
									<form action="controller" method="POST">
									<input type="hidden" name="command" value="userInfoUpdate"/>
										<tr>
											<td><fmt:message key="UserProfile.label.FName"/>:</td>
											<c:choose>
												<c:when test="${empty sessionScope.error_upadates }">
													<td><input type="text" class="form-control" name="first_name" placeholder="First name" value="${sessionScope.user.fName}" required></td>
												</c:when>
												<c:otherwise>
													<c:if test="${empty sessionScope.error_upadates['0'] }">
														<td>
														<div class="form-group has-success has-feedback">
															<input type="text" class="form-control" name="first_name" placeholder="First name" value="${sessionScope.user_update.fName}" required>
															<span class="glyphicon glyphicon-ok form-control-feedback"></span>
														</div>
														</td>
													</c:if>
													<c:if test="${not empty sessionScope.error_upadates['0'] }">
														<td>
														<div class="form-group has-error has-feedback">
															<input type="text" class="form-control has-error" name="first_name" placeholder="First name" value="${sessionScope.user_update.fName}"  required>
															<span class="glyphicon glyphicon-remove form-control-feedback"></span>
														</div>
														</td>
														<td style="color:red;">${sessionScope.error_upadates['0']}</td>
													</c:if>
												</c:otherwise>
											</c:choose>
										</tr>
										<tr>
											<td><fmt:message key="UserProfile.label.LName"/>:</td>											
											<c:choose>
												<c:when test="${empty sessionScope.error_upadates }">
													<td><input type="text" class="form-control" name="last_name" placeholder="Last name" value="${sessionScope.user.lName}" required></td>
												</c:when>
												<c:otherwise>
													<c:if test="${empty sessionScope.error_upadates['1'] }">
														<td>
															<div class="form-group has-success has-feedback">
															<input type="text" class="form-control has-succes" name="last_name" placeholder="Last name" value="${sessionScope.user_update.lName}" required>
															<span class="glyphicon glyphicon-ok form-control-feedback"></span>
														</div>
														</td>
													</c:if>
													<c:if test="${not empty sessionScope.error_upadates['1'] }">
														<td>
														<div class="form-group has-error has-feedback">
															<input type="text" class="form-control has-error" name="last_name" placeholder="Last name" value="${sessionScope.user_update.lName}" required>
															<span class="glyphicon glyphicon-remove form-control-feedback"></span>
														</div>
														</td>
														
														<td style="color:red;">${sessionScope.error_upadates['1']}</td>
														
													</c:if>
												</c:otherwise>
											</c:choose>
											
										</tr>
										<tr>
											<td><fmt:message key="UserProfile.label.Gender"/>:</td>
											<td>
												<div class="btn-group radio-group" data-toggle="buttons">
												<c:if test="${empty sessionScope.error_upadates }">
												<c:choose>
													<c:when test="${sessionScope.user.gender eq Male} ">
  														<label class="btn btn-default active">  												
  												  			<input type="radio" name="gender" id="option1" autocomplete="off" value="Male" checked> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female"> Female
  														</label>
  													</c:when>
  													<c:otherwise>
  														<label class="btn btn-default active">  												
  												 			 <input type="radio" name="gender" id="option1" autocomplete="off" value="Male"> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female" checked> Female
  														</label>
  													</c:otherwise>
  												</c:choose>
  												</c:if>
  												<c:if test="${not empty sessionScope.error_upadates }">
  													<c:if test="${empty sessionScope.error_upadates['2']} ">
  														<c:choose>
													<c:when test="${sessionScope.user.gender eq Male} ">
  														<label class="btn btn-default active">  												
  												  			<input type="radio" name="gender" id="option1" autocomplete="off" value="Male" checked> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female"> Female
  														</label>
  													</c:when>
  													<c:otherwise>
  														<label class="btn btn-default active">  												
  												 			 <input type="radio" name="gender" id="option1" autocomplete="off" value="Male"> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female" checked> Female
  														</label>
  													</c:otherwise>
  												</c:choose>
  													</c:if>
  													<c:if test="${not empty sessionScope.error_upadates['2'] }">
  														<c:choose>
													<c:when test="${sessionScope.user_update.gender eq Male} ">
  														<label class="btn btn-default active">  												
  												  			<input type="radio" name="gender" id="option1" autocomplete="off" value="Male" checked> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female"> Female
  														</label>
  													</c:when>
  													<c:otherwise>
  														<label class="btn btn-default active">  												
  												 			 <input type="radio" name="gender" id="option1" autocomplete="off" value="Male"> Male
  														</label>
  														<label class="btn btn-default">
  												 			 <input type="radio" name="gender" id="option2" autocomplete="off" value="Female" checked> Female
  														</label>
  													</c:otherwise>
  												</c:choose>
  													</c:if>
  												</c:if>  												
  												</div>
  											</td>
  											<c:if test="${not empty sessionScope.error_upadates['2'] }">
  												<td style="color:red;">
  													${sessionScope.error_upadates['2'] }
  												</td>
  											</c:if>
										</tr>
										<tr>
											<td><fmt:message key="UserProfile.label.DateOfBirth"/>:</td>
											
											<c:choose>
												<c:when test="${empty sessionScope.error_upadates }">
													<td><input type="date" class="form-control" name="birthday" value="${sessionScope.user.birthDate}" required></td>
												</c:when>
												<c:otherwise>
													<c:if test="${empty sessionScope.error_upadates['3'] }">
														<td>
															<div class="form-group has-success has-feedback">
																<input type="date" class="form-control has-succes" name="birthday" value="<fmt:formatDate pattern = "yyyy-MM-dd" 
         																value = "${sessionScope.user_update.birthDate}" />" required>
         													<span class="glyphicon glyphicon-ok form-control-feedback"></span>
         													</div>
         												</td>	
													</c:if>
													<c:if test="${not empty sessionScope.error_upadates['3'] }">
														<td>
														<div class="form-group has-error has-feedback">
														<input type="date" class="form-control has-error" name="birthday" value="<fmt:formatDate pattern = "yyyy-MM-dd" 
         														value = "${sessionScope.user_update.birthDate}" />" required>
         												<span class="glyphicon glyphicon-remove form-control-feedback"></span>
         												</div>
         												</td>
														<td style="color:red;">${sessionScope.error_upadates['3']}</td>
													</c:if>
												</c:otherwise>
											</c:choose>
										</tr>										
									
										<tr>
											<td><fmt:message key="UserProfile.label.TelNumb"/>:</td>
											
											<c:choose>
												<c:when test="${empty sessionScope.error_upadates }">
													<td><input type="tel" class="form-control" name="phone_number" placeholder="0XXXXXXXXX" pattern="^0+[0-9]{9}$" value="${sessionScope.user.telNumber}" required></td></td>
												</c:when>
												<c:otherwise>
													<c:if test="${empty sessionScope.error_upadates['4'] }">
														<td>
														<div class="form-group has-success has-feedback">
														<input type="tel" class="form-control has-succes" name="phone_number" placeholder="0XXXXXXXXX" value="${sessionScope.user_update.telNumber}" pattern="^0+[0-9]{9}$" required>
														<span class="glyphicon glyphicon-ok form-control-feedback"></span>
														</div>
														</td>
													</c:if>
													<c:if test="${not empty sessionScope.error_upadates['4'] }">
														<td>
														<div class="form-group has-error has-feedback">
														<input type="tel" class="form-control has-error" name="phone_number" placeholder="0XXXXXXXXX" value="${sessionScope.user_update.telNumber}" pattern="^0+[0-9]{9}$" required>
														<span class="glyphicon glyphicon-remove form-control-feedback"></span>
														</div>
														</td>
														<td style="color:red;">${sessionScope.error_upadates['4']}</td>
													</c:if>
												</c:otherwise>
											</c:choose>										
										</tr>
										<tr>
											<td colspan="2"><button class="btn btn-primary form-control" type="submit">Save Changes</button></td>
										</tr>
										</form>
										
										<form action="controller" method="post">
										<input type="hidden" name="command" value="userUpdatePassword" />
										<tr>
											<td><fmt:message key="Login.label.Password"/>:</td>
											<td><input type="password" class="form-control" name="password" placeholder="Password" pattern="^.{5,15}$" required></td>
										</tr>
										<tr>
											<td><fmt:message key="Login.label.ConfirmPassword"/>:</td>
											<td><input type="password" class="form-control" name="confirm_password" placeholder="Confirm password" pattern="^.{5,15}$" required></td>
										</tr>
										<c:if test="${not empty sessionScope.error_upadates_password}">
										<tr>
											<td>
												<span style="color:red;">${sessionScope.error_upadates_password}</span>
											</td>
										</tr>
										</c:if>
										<tr>
											<td colspan="2"><button class="btn btn-primary form-control" type="submit">Save Password</button></td>
										</tr>
										</form>
										
										<tr>
											<td colspan="2"><a href="controller?command=cancelUpdateUser" class="btn btn-primary form-control">Cancel</a></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						
						<c:if test="${sessionScope.userRole eq 'ADMIN'}">
						<!-- ADMIN MENU -->
						<div id="tab-slide-4" class="slide">
							<div class="option-line">              
            				  <a href="itemCreate.jsp" class="btn-add-item" title="Add Item"><span class="glyphicon glyphicon-plus"></span></a> 
            				  <h3><fmt:message key="UserProfile.label.AddNewItem"/></h3> 
            				 </div>             				 
            				 <div class="option-line">              
            				  <p class="icon-down"><span class="glyphicon glyphicon-chevron-down"></span></p>
            				  <h3><fmt:message key="UserProfile.label.HomeCarousel"/></h3>
            				 	 <div class="option-block">            				 	 	
            				 	 	<button type="button" class="form-control btn-primary" style="margin-bottom: 15px;" data-toggle="modal" data-target="#mini-form">
									 	Add carousel item
									</button>
							        <div class="input-group marg-bot">
							        	<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>	
							        	<input type="text" id="search-1" target-table="table-1" class="form-control table-searching" placeholder="Search">
							        	<div class="input-group-btn">
			    					    	<select name="option-for-search-table-1" class="selectpicker search-select form-control">
  			    					    		<option value="0">Picture Name</option>
  			    					    		<option value="1">Information</option>
			    					    	</select>
			    					    </div>	
							        </div>
							        <my:MyTag/>
            				 	 	<div class="panel panel-default panel-table">
            				 	 		<div class="panel-body">            				 	 			      						   
											<table id="table-1" class="table table-striped table-bordered table-list">
												<thead>
													<tr>
														<th>Picture Name</th>
														<th>Information</th>
														<th>Delete</th>
													</tr>
												</thead>												
												<tbody>
												<c:choose>
												<c:when test="${ not empty CarouselItemList}">
													<c:forEach items="${CarouselItemList}" var="cItem">
													<tr>
														<td>${cItem.name}</td>
														<td>${cItem.header }</td>
														<td><a class="btn btn-danger" href="controller?command=removeCarouselItem&targ-item=${cItem.id }" title="Remove"><span class="glyphicon glyphicon-remove"></span></a></td>
													</tr>
													</c:forEach>
												</c:when>
												<c:otherwise>
													<tr>
														<td colspan="3" align="center">Oops, we haven't carousel items</td>
													</tr>
												</c:otherwise>
												</c:choose>													
												</tbody>
											</table>
										</div>																		
									</div>
            				 	 </div>            				 	
            				 </div>
            				 <div class="option-line">              
            				  <p class="icon-down"><span class="glyphicon glyphicon-chevron-down"></span></p>
            				  <h3><fmt:message key="UserProfile.label.Orders"/></h3>
            				 	 <div class="option-block">
            				 	 	<div class="input-group marg-bot">
							        	<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>	
							        	<input type="text" id="search-2" target-table="table-2" class="form-control  table-searching" placeholder="Search">
							        	<div class="input-group-btn">
			    					    	<select name="option-for-search-table-2" class="selectpicker search-select form-control">
  			    					    		<option value="0">User Name</option>
  			    					    		<option value="1">Items</option>
  			    					    		<option value="2">Total Price</option>
  			    					    		<option value="3">Date</option>
  			    					    		<option value="4">Status</option>
			    					    	</select>
			    					    </div>
							        </div>
            				 	 	<div class="panel panel-default panel-table">
            				 	 		<div class="panel-body">           						   
											<table id="table-2" class="table table-striped table-bordered table-list">
												<thead>
													<tr>
														<th>User Name</th>
														<th>Items</th>
														<th>Total Price</th>
														<th>Date</th>
														<th>Status</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>User Name</th>
														<th>Items</th>
														<th>Total Price</th>
														<th>Date</th>
														<th>Status</th>
													</tr>
												</tfoot>
												<tbody>
												<c:forEach items="${requestScope.AllOrders}" var="order">
									<c:choose>
										<c:when test="${ order.status eq 'Paid'}">
											<tr class="succes">
										</c:when>
										<c:when test="${order.status eq 'Aborted'}">
											<tr class="danger">
										</c:when>
										<c:otherwise>
											<tr class="info">
										</c:otherwise>	
									</c:choose>
									<td>${order.client.fName } ${order.client.lName }</td>
									<td>
									<c:forEach items="${order.orderParts}" var="orderPart">										
										${orderPart.item.name} ( ${orderPart.item.price} x  ${orderPart.count } = ${orderPart.total_price })
										<br>
									</c:forEach>
									</td>
									<td>${order.totalPrice }</td>
									<td><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${order.orderDate}" />
									<td>
									<c:choose>
										<c:when test="${ order.status eq 'Registred'}">
										<form action="controller" method="POST">
											<input type="hidden" name="command" value="setOrderStatus">
											<input type="hidden" name="target-order" value="${order.id }">
											<input type="hidden" name="action" value="Pay">
											<button type="submit" class="btn btn-success" title="Set Paid"><span class="glyphicon glyphicon-ok"></span></button> 
										</form>
										<form action="controller" method="POST">
											<input type="hidden" name="command" value="setOrderStatus">
											<input type="hidden" name="target-order" value="${order.id }">
											<input type="hidden" name="action" value="Abort">
											<button type="submit" class="btn btn-danger" title="Set Aborted"><span class="glyphicon glyphicon-ban-circle"></span></button> 
										</form>
										</c:when>
										<c:otherwise>
											 ${order.status }											
										</c:otherwise>	
									</c:choose>
									</td>
									
									</tr>
								</c:forEach>
												</tbody>
											</table>
										</div>										
              						</div>
              					</div>
            				 </div>
            				 <div class="option-line">              
            				  <p class="icon-down"><span class="glyphicon glyphicon-chevron-down"></span></p>
            				  <h3><fmt:message key="UserProfile.label.Users"/></h3>
            				 	 <div class="option-block">
            				 	 	<div class="input-group marg-bot">
							        	<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>	
							        	<input type="text" id="search-3" target-table="table-3" class="form-control  table-searching" placeholder="Search">	
							        	<div class="input-group-btn">
			    					    	<select name="option-for-search-table-3" class="selectpicker search-select form-control">
  			    					    		<option value="0">User Name</option>
  			    					    		<option value="1">Gender</option>
  			    					    		<option value="2">Email</option>
  			    					    		<option value="3">Number</option>
  			    					    		<option value="4">Total Order Price</option>
  			    					    		<option value="5">Join Date</option>
			    					    	</select>
			    					    </div>
							        </div>
            				 	 	<div class="panel panel-default panel-table">
            				 	 		<div class="panel-body">           						   
											<table id="table-3" class="table table-striped table-bordered table-list">
												<thead>
													<tr>
														<th>User Name</th>
														<th>Gender</th>
														<th>Email</th>
														<th>Number</th>
														<th>Total Order Price</th>
														<th>Join Date</th>
														<th>Role</th>
													</tr>
												</thead>
												<tfoot>
													<tr>
														<th>User Name</th>
														<th>Gender</th>
														<th>Email</th>
														<th>Number</th>
														<th>Total Order Price</th>
														<th>Join Date</th>
														<th>Role</th>
													</tr>
												</tfoot>
												<tbody>
													<c:forEach items="${requestScope.UsersList }" var = "curUser">
												 	<c:choose>
												  		<c:when test="${curUser.roleId eq '2'}"> 
												  				<tr	class="info">
														</c:when>
														 <c:when test="${curUser.isBanned eq true}">
																 <tr class="danger">
														 </c:when>
													  		<c:otherwise>
													   			<tr> 
													  	 </c:otherwise>
													 </c:choose>
													<td>${curUser.fName } ${curUser.lName }</td>
													<td>${curUser.gender }</td>
													<td>${curUser.email }</td>
													<td>${curUser.telNumber }</td>													
													<td>${curUser.totalPrice }</td>
													<td>${curUser.joinDate }</td>
													<td>
														<c:choose>
														<c:when test="${curUser.roleId eq '2' }">
															Admin
														</c:when>
														<c:when test="${curUser.isBanned eq true}">
															<a class="btn btn-default" href="controller?command=banChange&targ-user=${curUser.id }&unBan=true" title="Unban"><span class="glyphicon glyphicon-ok-circle"></span></a>
														</c:when>
														<c:otherwise>
															<a class="btn btn-danger" href="controller?command=banChange&targ-user=${curUser.id }" title="Ban"><span class="glyphicon glyphicon-ban-circle"></span></a>
															<a class="btn btn-info" href="controller?command=upgradeUser&targ-user=${curUser.id }" title="Upgrade to admin"><span class="glyphicon glyphicon-arrow-up"></span></a>
														</c:otherwise>
														</c:choose>
													</td>
												</tr>
												</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
            				 	 </div>            				 	
            				 </div>          				             				  
						</div>
						</c:if>
							</c:if>
							</div>
							</div>
						
					</div>
				</div>
			</div>

</body>
</html>