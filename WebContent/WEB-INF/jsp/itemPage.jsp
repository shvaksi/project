
<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

<c:set var="title" value="Item #{$requestScope.item.id }" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
	
	<div class="container-fluid page-wrapper">
	<c:set var="company_name" value="My Company"></c:set>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<!-- Part of content -->
			<div class="main-content">
				<div class="container-fluid">
				<c:if test="${sessionScope.userRole eq 'ADMIN' }">
				<div class="option-item-btn">
					<a class="btn btn-primary" title="Edit Item" href="controller?command=itemUpdate&targ-item=${item.id}"><span class="glyphicon glyphicon-pencil"></span></a>
					<a class="btn btn-primary" title="Delete Item" href="controller?command=itemDelete&targ-item=${item.id}"><span class="glyphicon glyphicon-remove"></span></a>
				</div>	
				</c:if>
				<div class="cols">									
					<div class="left-col">
						<img class="img-responsive" src="${item.avatar }">
					</div>
					<div class="right-col">
						<div class="name">
							<h1> ${item.name} </h1>
						</div>
						<p><span class="added-time">Added date: <fmt:formatDate type = "both" 
         dateStyle = "short" timeStyle = "short" value = "${item.addDate}" /></span><p>
						<div class="item-price">
							<span class="price"> ${item.price} <span class="currency"><fmt:message key="Currency.Uah"/></span></span>
						</div>
						<div class="btn-buy">
						<form action="controller" method="POST">
									<input type="hidden" name="command" value="basketAddItem">
									<input type="hidden" name="targ-item" value="${item.id}">
							<button class="btn btn-primary" type="submit" ><span class="glyphicon glyphicon-shopping-cart"></span></button>
						</form>
						</div>
						<div class="tabs">
							<div class="tab-menu">
								<ul class="nav nav-tabs">
									<li data-open="1" class="active"><a href="#">Characteristics</a></li>
  									<li data-open="2"><a href="#">Description</a></li>
								</ul>
							</div>
							<div class="tab-slides">
								<div id="tab-slide-1" class="slide active">
									<table class="table">
										<thead>
											<th scope="col">#</th>
											<th scope="col">Characteristic</th>
											<th scope="col">Value</th>
										</thead>
										<tbody>											
											<tr>
												<th scope="row">1</th>
												<td>Category</td>
												<td>
												<c:forEach items="${applicationScope.itemCategories}" var="category">
													<c:if test="${category.id eq item.category_id}">
														${category.name }
													</c:if>
												</c:forEach>
												</td>												
											</tr>
											<tr>
												<th scope="row">2</th>
												<td>Manufacturer</td>
												<td>
												<c:forEach items="${applicationScope.itemCountries}" var="country">
													<c:if test="${country.id eq item.country_id}">
														${country.name }
													</c:if>
												</c:forEach>
												</td>												
											</tr>
										</tbody>
									</table>
								</div>
								<div id="tab-slide-2" class="slide">${item.description}</div>
							</div>						
						</div>
					</div>
				</div>
				<div class="comments">
				<c:if test="${not empty sessionScope.userRole}">
					<div class="comment-send">
						<form action="controller" method="POST">
							<input type="hidden" name="command" value="addComment">
							<input type="hidden" name="targ-item" value="${item.id }">
							<div class="form-group">							
								<textarea class="form-control" name = "comment" rows="5" placeholder="Write your comment..."></textarea>
								<button class="btn btn-primary" type="submit">Send</button>
							</div>
						</form>						
					</div>
					</c:if>
					<c:choose>
					<c:when test="${not empty requestScope.itemComments }">
						<c:forEach items="${requestScope.itemComments }" var = "comment">
						<div class="comment">
						<div class="user-data">
							<c:if test="${sessionScope.userRole eq 'ADMIN' or sessionScope.user.id eq comment.owner.id}">
								<form action="controller" method="POST">
									<input type="hidden" name="command" value="deleteComment">
									<input type="hidden" name="targ-comment" value="${comment.id }">
									<button type="submit" class="delete-btn" title="Delete"><span class="glyphicon glyphicon-remove"></span></button>
								</form>
							</c:if>
							<p class="message-date"><fmt:formatDate type = "both" dateStyle = "short" timeStyle = "short" value = "${comment.addDate}" /></p>
							<a href="controller?command=userProfile&targ-user=${comment.owner.id }" class="user-link" title="${comment.owner.fName } ${comment.owner.lName } "><img src="${comment.owner.avatar}">
								<p>${comment.owner.fName } ${comment.owner.lName } </p>
							</a>
						</div>
						<div class="message">
							<p>${comment.comment }</p>
						</div>
						
					</div>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<h4>This item haven't comments</h4>
					</c:otherwise>
					</c:choose>
					
				</div>
			</div>
				</div>
			</div>
	</div>
</body>
</html>