<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

<c:set var="title" value="Item Update" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
<c:choose>
<c:when test = "${sessionScope.userRole eq 'ADMIN'}">
	<div class="container-fluid page-wrapper">
	<c:set var="company_name" value="My Company"></c:set>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<!-- Part of content -->
			<div class="main-content">
				<div class="container-fluid">
				<div class="omb_login">
			<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6" style="z-index: 0;">				  
			<form class="omb_loginForm" autocomplete="off" method="POST" action="controller" enctype="multipart/form-data">
				<input type="hidden" name="command" value="updateItemImage">
				<input type="hidden" name="targ-item" value="${requestScope.item.id}">
					
					 <div class="form-group">
						<label for="item-pict">Item Picture</label>
  						<input type="file" class="form-control-file" accept="image/jpeg,image/png" name="item-pict"  required>
  						<c:choose>
  							<c:when test="${empty sessionScope.updateItemError}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemError}</span>
  							</c:otherwise>
  						</c:choose>
  						<button class="btn btn-lg btn-primary btn-block" type="submit">Update Item Image</button>
					</div>
			</form>
			<form class="omb_loginForm" action="controller" method="POST" autocomplete="off">
					<input type="hidden" name="command" value="updateItemInfo">
					<input type="hidden" name="targ-item" value="${requestScope.item.id}">
					<div class="form-group">
						<label for="item-price">Item name</label>
						<input type="text" class="form-control" name="item-name" value="${requestScope.item.name}" required placeholder="Item name">
					</div>
					<c:choose>
  							<c:when test="${empty sessionScope.updateItemInfoErrors or empty sessionScope.updateItemInfoErrors['0']}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemInfoErrors['0']}</span>
  							</c:otherwise>
  						</c:choose>

					<div class="form-group">
						<label for="item-price">Item price</label>
						<input type="number" class="form-control" min="1" max="10000" value="${ requestScope.item.price}" required name="item-price" placeholder="Item price">
					</div>
					<c:choose>
  							<c:when test="${empty sessionScope.updateItemInfoErrors or empty sessionScope.updateItemInfoErrors['1']}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemInfoErrors['1']}</span>
  							</c:otherwise>
  						</c:choose>

					<div class="form-group">
						<label for="item-category">Item category</label>
						<select class="form-control" name="item-category">
  						  <c:forEach items="${applicationScope.itemCategories}" var="curItem" >
  						  <option value="${curItem.id}" <c:if test="${requestScope.item.category_id eq curItem.id}">selected</c:if>>${curItem.name}</option>
  						  </c:forEach>
  						</select>
					</div>
					<c:choose>
  							<c:when test="${empty sessionScope.updateItemInfoErrors or empty sessionScope.updateItemInfoErrors['2']}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemInfoErrors['2']}</span>
  							</c:otherwise>
  						</c:choose>

					<div class="form-group">
						<label for="manufacturer">Manufacturer</label>
						<select class="form-control" name="manufacturer">
  						 <c:forEach items="${applicationScope.itemCountries}" var="curItem" >
  						  <option value="${curItem.id}" <c:if test="${requestScope.item.country_id eq curItem.id}">selected</c:if>>${curItem.name}</option>
  						  </c:forEach>
  						</select>
					</div>
					<c:choose>
  							<c:when test="${empty sessionScope.updateItemInfoErrors or empty sessionScope.updateItemInfoErrors['3']}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemInfoErrors['3']}</span>
  							</c:otherwise>
  						</c:choose>

					<div class="form-group">
						<label for="item-description">Item description</label>					
						<textarea class="form-control" name="item-description" rows="5" required placeholder="Item description...">${requestScope.item.description}	</textarea>
					</div>
					<c:choose>
  							<c:when test="${empty sessionScope.updateItemInfoErrors or empty sessionScope.updateItemInfoErrors['4']}">
  								<span class="help-block"></span>
  							</c:when>
  							<c:otherwise>
  								<span class="help-block">${sessionScope.updateItemInfoErrors['4']}</span>
  							</c:otherwise>
  						</c:choose>

					<button class="btn btn-lg btn-primary btn-block" type="submit">Update Item</button>
				</form>	
				</div>
				</div>
				</div>
				</div>
			</div>
	</div>
	</c:when>
	<c:otherwise>
		<c:redirect url="controller?command=userProfile"></c:redirect>
 	</c:otherwise>
	</c:choose>
</body>
</html>