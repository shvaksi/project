<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

<c:set var="title" value="Error" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
	
	<div class="container-fluid page-wrapper">
	<c:set var="company_name" value="My Company"></c:set>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<!-- Part of content -->
			<div class="main-content">
				<div class="container-fluid">
				<c:set var="code" value="${requestScope['javax.servlet.error.status_code']}"/>
				<c:set var="message" value="${requestScope['javax.servlet.error.message']}"/>
				
				<%-- this way we get the exception --%>
				<c:set var="exception" value="${requestScope['javax.servlet.error.exception']}"/>
				
				<c:if test="${not empty code}">
					<h3>Error code: ${code}</h3>
				</c:if>			
				
				<c:if test="${not empty message}">
					<h3>Message: ${message}</h3>
				</c:if>
				
				<%-- if get this page using forward --%>
				<c:if test="${not empty errorMessage and empty exception and empty code}">
					<h3>Error message: ${errorMessage}</h3>
				</c:if>	
				
				<%-- this way we print exception stack trace --%>
				<c:if test="${not empty exception}">
					<hr/>
					<h3>Stack trace:</h3>
					<c:forEach var="stackTraceElement" items="${exception.stackTrace}">
						${stackTraceElement}
					</c:forEach>
				</c:if>	
				
				</div>
			</div>
	</div>
</body>
</html>