<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

<c:set var="title" value="Catalog" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
<div class="container-fluid page-wrapper">
	<c:set var="company_name" value="My Company"></c:set>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<!-- Part of content -->
			<div class="main-content">
				<div class="container-fluid">
				<div class="cols">
					<div class="left-col">
				<div class="filters">
					<div class="filter ">
						<div class="filter-name">
							<h4>Filters</h4>
						</div>
					</div>
					<form action="controller">
					<input type="hidden" name="command" value="filterItem">
					<div class="filter">
						<div class="filter-header">
							<h4>Category</h4>
						</div>
						<div class="filter-content">
							<ul class="list-group">
							<c:forEach items="${applicationScope.itemCategories}" var="curCategory">
								<li class="list-group-item">
                			        ${curCategory.name }
                			        <div class="material-switch my-chekcbox pull-right">
                			        <c:choose>               			        	
                			        	<c:when test="${ not empty sessionScope.appliedCategoryFilters[curCategory.id-1]}">
                			        		<input id="category-${curCategory.id }" name="${curCategory.name }" type="checkbox" checked/>
                			        	</c:when>
                			        <c:otherwise>
                			        	<input id="category-${curCategory.id }" name="${curCategory.name }" type="checkbox"/>
                			        </c:otherwise>
                			        </c:choose>
                			            
                			            <label for="category-${curCategory.id }" class="label-primary"></label>
                			        </div>
                			    </li>
							</c:forEach>  			    
                			</ul>
						</div>
					</div>
					<div class="filter">
						<div class="filter-header">
							<h4>Manufacturer</h4>
						</div>
						<div class="filter-content">
							<ul class="list-group">
							<c:forEach items="${applicationScope.itemCountries}" var="curCountry">
								<li class="list-group-item">
                			        ${curCountry.name }
                			        <div class="material-switch my-chekcbox pull-right">
                			        	<c:choose>               			        	
                			        	<c:when test="${ not empty sessionScope.appliedCountryFilters[curCountry.id-1]}">
                			            	<input id="country-${curCountry.id }" name="${curCountry.name }" type="checkbox" checked/>
                			        	</c:when>
                			        <c:otherwise>
                			            	<input id="country-${curCountry.id }" name="${curCountry.name }" type="checkbox"/>
                			        </c:otherwise>
                			        </c:choose>
                			            <label for="country-${curCountry.id }" class="label-primary"></label>
                			        </div>
                			    </li>
							</c:forEach>  			    
                			</ul>
						</div>
					</div>
					<div class="filter">
						<div class="filter-header">
							<h4>Price</h4>
						</div>
						<div class="filter-content">
							<div class="conteiner-for-half-block">
								<span class="price"><span class="currency">UAH</span></span>
								<div class="half-num-block">
									<label for="min-price">From</label>								
									<input class="form-control" type="number" name="min-price" value="${sessionScope.minPrice }" />
								</div>
								-
								<span class="price"><span class="currency">UAH</span></span>
								<div class="half-num-block">
									<label for="max-price">To</label>	
									<input class="form-control" type="number" name="max-price" value="${sessionScope.maxPrice }" />
								</div>	
							</div>																			
						</div>
					</div>
					<button class="btn btn-primary form-control" type="submit">Apply</button>
				</form>
				</div>
			</div>
			<div class="right-col">
				<div class="catalog-content">
				<c:if test="${not empty sessionScope.searchByName and not empty sessionScope.searchingCategory}">
					<h3>
						Results by: ${sessionScope.searchByName} in ${sessionScope.searchingCategory.name}
					 </h3>
				 </c:if>
					<div class="sort-options">
						<p>Sort by: </p>						
						<div class="option">
						<c:choose>
							<c:when test="${not empty requestScope.sortedByName }">
								<a href="controller?command=catalogSort&sortBy=Name&reverse=true"><p>Name <span class="glyphicon glyphicon-sort-by-alphabet-alt"></span></p></a>
							</c:when>
							<c:otherwise>
								<a href="controller?command=catalogSort&sortBy=Name"><p>Name <span class="glyphicon glyphicon-sort-by-alphabet"></span></p></a>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="option">
						<c:choose>
							<c:when test="${not empty requestScope.sortedByPrice }">
								<a href="controller?command=catalogSort&sortBy=Price&reverse=true"><p>Price <span class="glyphicon glyphicon-sort-by-order"></span></p></a>
							</c:when>
							<c:otherwise>
								<a href="controller?command=catalogSort&sortBy=Price"><p>Price <span class="glyphicon glyphicon-sort-by-order-alt"></span></p></a>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="option">
						<c:choose>
							<c:when test="${not empty requestScope.sortedByDate }">
								<a href="controller?command=catalogSort&sortBy=Date&reverse=true"><p>Date <span class="glyphicon glyphicon-sort-by-attributes-alt"></span></p></a>
							</c:when>
							<c:otherwise>
								<a href="controller?command=catalogSort&sortBy=Date"><p>Date <span class="glyphicon glyphicon-sort-by-attributes"></span></p></a>
							</c:otherwise>
						</c:choose>
						</div>
					</div>
					
					<div class="catalog-items">
						<c:choose>
							<c:when test="${not empty sessionScope.itemCatalog }">
							<c:forEach items="${sessionScope.itemCatalog }" var="item">
							<div class="gallery_product col-xs-6">
           			    		<div class="img-btn-buy">
           			        		<form action="controller" method="POST">
									<input type="hidden" name="command" value="basketAddItem">
									<input type="hidden" name="targ-item" value="${item.id}">
									<button type="Submit">${item.price}  <fmt:message key="Currency.Uah"/> <span class="glyphicon glyphicon-shopping-cart"></span> </button>		
           			       		 	</form>
           			       		 </div> 
           			        	<a href="controller?command=itemPage&targ-item=${item.id}"><img class="img-responsive" alt="" src="${item.avatar }" /></a>
           			        		<div class='img-info'>
           			        		<a href="controller?command=itemPage&targ-item=${item.id}">
           			        			<div class="item-info">
           			            			<h4> ${item.name }</h4>
											<p>${item.description } </p>
										</div>
									</a>
									<form action="controller" method="POST">
									<input type="hidden" name="command" value="basketAddItem">
									<input type="hidden" name="targ-item" value="${item.id}">
									<button type="Submit">${item.price}  <fmt:message key="Currency.Uah"/> <span class="glyphicon glyphicon-shopping-cart"></span> </button>		
           			       		 	</form>
           			       		 	</div>           			    
           			 		</div>
							</c:forEach>
							</c:when>
							<c:otherwise>
								<h3>Sorry, catalog is empty.</h3>
							</c:otherwise>
						</c:choose>
						
					</div>
				</div>
				</div>
			</div>
			</div>
			</div>
			</div>
</body>
</html>