<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<html>

	<c:set var="title" value="Simple Name" />
	<%@ include file="/WEB-INF/jspf/head.jspf" %>

<body>
	<div class="container-fluid page-wrapper">
		<c:set var="company_name" value="My Company"></c:set>
		<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<div class="main-content">
		<div class="container">
		<c:choose>	
		
			<c:when test= "${ empty sessionScope.user}" >		
			<div class="omb_login">				
			<div class="row omb_row-sm-offset-3">
			<div class="col-xs-12 col-sm-6" style="z-index: 0;">			
			    <form class="omb_loginForm login-form" action="controller" autocomplete="off" method="POST">
					<input type="hidden" name="command" value="loginUser">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input type="email" class="form-control" name="email" placeholder="email address"  required>
					</div>
					<span class="help-block"></span>					
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						<input  type="password" class="form-control" name="password" placeholder="Password"  required>
					</div>
					<c:choose>
					<c:when test="${not empty sessionScope.loginErrorMessage}">
                    	<span class="help-block">	${sessionScope.loginErrorMessage}	</span>
					</c:when>
						<c:otherwise>
							<span class="help-block"></span>
						</c:otherwise>
					</c:choose>
					<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
				</form>
			</div>
		</div>

    	<div class="row omb_row-sm-offset-3 login-form">
			<div class="col-xs-12 col-sm-3">
				<p class="omb_forgotPwd">
					<a id="reg-form-open" href="registry.jsp">Create new account</a>
				</p>
			</div>
		</div>	
		</div>    	
	</c:when>
	
	<c:otherwise>
		<c:redirect url="controller?command=userProfile"></c:redirect> 
	</c:otherwise>
	
</c:choose>
		</div>
	</div>
	</div>
</body>
</html>