<%@ include file="/WEB-INF/jspf/directive/page.jspf" %>
<%@ include file="/WEB-INF/jspf/directive/taglib.jspf" %>
<%@ taglib prefix = "my" uri = "/WEB-INF/myTags.tld"%>
<html>

<c:set var="title" value="Simple Name" />
<%@ include file="/WEB-INF/jspf/head.jspf" %>
	
<body>
	
	<div class="container-fluid page-wrapper">
	<c:set var="company_name" value="My Company"></c:set>
	<%@ include file="/WEB-INF/jspf/header.jspf" %>
		<!-- Part of content -->
			<div class="main-content">
				<div class="container-fluid">
				
        		<my:MyTag/>
				<c:if test="${not empty CarouselItemList }">
				<section class="section-white relative-carousel">
        		    <div class="carousel-cont">        		
        		    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        		      <!-- Indicators -->
        		      <ol class="carousel-indicators">
        		      	<c:forEach items="${CarouselItemList }" var="cItem" varStatus="count">
        		      	<li data-target="#carousel-example-generic" data-slide-to="${count.index}" <c:if test="${count.index eq '0' }">  class="active"</c:if>></li>
        		      	</c:forEach>
        		      </ol>
        		
        		      <!-- Wrapper for slides -->
        		      <div class="carousel-inner">
						<c:forEach items="${CarouselItemList }" var="cItem" varStatus="count">
        		        <c:choose>
        		        <c:when test="${count.index eq '0'}"> 
        		                <div class="item active">
        		        </c:when>
        		        <c:otherwise>
        		       			 <div class="item">
        		        </c:otherwise>
        		        </c:choose>
        		          <img src="${cItem.image }" alt="...">
        		          <div class="carousel-caption">
        		            <h2>${cItem.header }</h2>
        		          </div>
        		        </div>
        		        </c:forEach>
        		        
        		      </div>
        			<c:if test="${fn:length(CarouselItemList) gt 1 }">
        		      <!-- Controls -->
        		      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        		        <span class="glyphicon glyphicon-chevron-left"></span>
        		      </a>
        		      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        		        <span class="glyphicon glyphicon-chevron-right"></span>
        		      </a>
        		      </c:if>
        		    </div>
        		   </div>
        		</section>
        		</c:if>
        		
				<c:if test="${not empty newestItems }">
					<c:forEach items="${newestItems }" var="item">
						<div class="gallery_product col-sm-3 col-xs-6">
           			    	<div class="img-btn-buy">
									<form action="controller" method="POST">
									<input type="hidden" name="command" value="basketAddItem">
									<input type="hidden" name="targ-item" value="${item.id}">
									<button type="Submit">${item.price}  <fmt:message key="Currency.Uah"/> <span class="glyphicon glyphicon-shopping-cart"></span> </button>		
           			       		 	</form>  
           			        </div> 
           			        <a href="controller?command=itemPage&targ-item=${item.id}"><img class="img-responsive" alt="" src="${item.avatar }" /></a>
           			        		<div class='img-info'>
           			        		<a href="controller?command=itemPage&targ-item=${item.id}">
           			        			<div class="item-info">
           			            			<h4> ${item.name }</h4>
											<p>${item.description } </p>
										</div>
									</a>
									<form action="controller" method="POST">
									<input type="hidden" name="command" value="basketAddItem">
									<input type="hidden" name="targ-item" value="${item.id}">
									<button type="Submit">${item.price}  <fmt:message key="Currency.Uah"/> <span class="glyphicon glyphicon-shopping-cart"></span> </button>		
           			       		 	</form>
           			       		 	</div>           			    
           			 </div>
					</c:forEach>
				</c:if>
           			
            	
            	</div>
			</div>


		</div>




</body>
</html>